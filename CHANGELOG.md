### Ver 0.20

##### 0.20.6
- Using Streamer Retriever 0.20.4
  - Bugfix: State value made accessible for GUI, now token regex workds again

##### 0.20.5
- Using Streamer Retriever 0.20.3
  - Bugfix: Twitch token creation should work again

##### 0.20.4
- Using Streamer Retriever 0.20.2
  - Bugfix: shows all twitch follows even if more than 20

##### 0.20.3
- Using Streamer Retriever 0.20.1
- Show error message if token URL incorrect
- Windows configs -> ./configs
- With frames as default
- Fix table data not updating if no icons used
- Fix follows list weirness
- Fix custom themes not loading

##### 0.20.2
- Fixed theme related crash when creating token

##### 0.20.1
- Dark icons updated
- Fixing memory leaking
- Show changelog tray entry

##### 0.20.0
- **v0.10 and earlier version configurations are not compatible**
  - follows.csv file is still compatible
- Rewritten for Twitch Helix API
- Sorting streams
- Load your streamer follows from Twitch
- Frameless mode
- Theme and new icons
- Redesigned choosing windows
- New keybinds
- New icons
- Icon download in thread

### Ver 0.10

##### 0.10.1
- Always open chat option
- Bugfix column header when no streams online
- Bugfix removed "Recently online" message if closing settings just after
opening the program

##### 0.10.0
- A lot of small UI changes
- Added chat and chat&play buttons
- Filter hidden when not used
- Systemtray menu supports icons
- Preview button for settings
- Option to show grid and column headers in main window
- Bugfix traymenu loads only once

### Ver 0.9

##### 0.9.1
- Security update to Streamer Retriever and Streamer Retriever-GUI
  - urllib.request url regex checked before run
  - url regex check before play

##### 0.9.0
- UI changes for Settings
- Change main window table font
- Change main window table row height
- Main window row margin
- Enable streamer icons
  - Adds streamer_icons as a column option
  - Set icon size


### Ver 0.8

##### 0.8.0
- Makefile creates all needed folders if necessary
- PKGBUILD for Arch
- Improving tooltips
- Context Menu Help texts
- Bugfix filter autoplay not starting
- Bugfix margin=0 value


### Ver 0.7

##### 0.7.0
- Png icons
- Make install and uninstall
- Bugfix margin accepts value 0


### Ver 0.6

##### 0.6.0
- Makefile
- Choices update main window with their own OK instead of wating OK in settings
- Adding theme supported icons
- Error checking follows list
- Editing follow inputs current value into editing box
- Requires Streamer Retriever v0.8.1
- Bugfix no editing table items when choosing
- Bugfix no multichoises with stretch choosing
- Bugfix autostarting disabled with default webbrowser


### Ver 0.5

##### 0.5.0
- Mainwindow resizing
- Tooltips for choosing
- Slighty bigger numbers in icon
- Filter works with added columns
- Bugfix when saving with empty config file
- Bugfix for autostarting
- Bugfix for filter


### Ver 0.4

##### 0.4.0
- Customize what is shown in main window
- Change main window columns order in main window editing
- Set which main window column should stretch to fit window size
- Requires Streamer Retriever ver 0.7.9


### Ver 0.3

##### 0.3.2
- Fix for delay between autostarting function

##### 0.3.1
- Quick fix for settings if config file is empty

##### 0.3.0
- Double clicking stream starts playing
- Major fix for filter autoplaying
  - No more multiplay streams if you keep writing extra characters
  - Autoplay starts only when adding characters
- Option to list streams that you want to start automatically


### Ver 0.2

##### 0.2.0
- Systemtray icon shows how many streams online
- Added "Hide" to toolbar actions with hotkey <Ctrl+H>


### Ver 0.1

##### 0.1.2
- Initial release
- New icons
