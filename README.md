<img
    src="/res/icons/streamerretriever.svg"
    alt="icon" align="left" height="48" width="48" />

# Streamer Retriever GUI

A program that helps you keep track of streamers you follow and start their
streams with a media player of your own choice.

GUI for [Streamer Retriever][1].

![GUI Image](/res/screenshot/gui.png)

- Get a notification when streamer goes online
- See how many of your follows are online from trayicon
- Choose streamers whose streams start playing automatically
- Start playing stream, chat or both
- Start streams and chat with a program of your choice
  - Your default webbrowser is used if players are not set
- Filter streams by pressing `<Ctrl+F>`
- Change what data is shown from streams.
- Sort table by pressing columns, or sort database manually from Database
- Change column order by drag&drop and save order with `<Ctrl+S>`

![Systemtray Image](/res/screenshot/tray.png)
![Message Image](/res/screenshot/msg.png)


## Install

1. Linux
   1. Arch Linux download PKGBUILD from [releases][7] (AUR pending)
      - Install with `makepkg -i`
   2. Debian based distros download .deb from [releases][7] and install
   3. [Tarball][3] `sudo make install`
   4. [Tarball][3] unpack and start `streamerretriever-gui.py`
2. Windows
   1. [Binary version for Windows][6]
      - Made with [cx_Freeze][11]
   2. [Tarball][3] unpack and start `streamerretriever-gui.py`


### Tarball

First install the [requirements][4].


#### Requirements

* Python 3.8 or newer
  * In Ubuntu `sudo apt install python3`
  * In Windows install [Python 3](https://www.python.org/)
    * Consider selecting `Add Python to PATH` during install
* [PySide2][2]
  * `pip install --user --upgrade PySide2`


#### Using Tarball

1. Download latest [tarball release][7] under the `Packages`
2. Unpack tarball files (do not separate the files into different folders)


##### Linux

3. `cd` into the tarball folder
4. Run `sudo make install`
   1. Alternatively just run `./streamerretriever-gui.py`

> Note: To [uninstall][9] run `sudo make uninstall`


##### Windows

3. Goto the folder where you unpacked the tarball
4. Double click `streamchecker-gui.py`

### Windows Binary

Windows binary was made with [cx_Freeze][11].

1. Download latest [Windows binary release][7] under the `Packages`
2. Unpack the 7zip files
3. Run `Streamer Retriever-GUI.exe`


## Uninstall

### Uninstall Linux

##### Arch Linux

`sudo pacman -Rns streamerretriever-gui`

##### Debian based

`sudo apt uninstall streamerretriever-gui`

##### Tarball

If you did the [Linux tarball][10] you can use the following method to
uninstall.

1. `cd` into your tarball folder
2. Run `sudo make uninstall`

[1]: https://gitlab.com/obtusescholar/streamerretriever
[2]: https://wiki.qt.io/Qt_for_Python
[3]: #tarball
[4]: #requirements
[5]: #download-tarball
[6]: #windows-binary
[7]: ../../releases
[8]: ../../wikis/ToDo
[9]: #uninstall-linux-tarball
[10]: #using-tarball
[11]: https://github.com/marcelotduarte/cx_Freeze
