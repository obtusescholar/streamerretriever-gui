NAME = streamerretriever-gui
SRC = streamerretriever_gui
REQ = streamerretriever
RES = res
ICONS = $(RES)/icons
THEMES = $(RES)/themes
AUR = bin/aur

ifeq ($(PREFIX),)
    PREFIX := /opt
endif
ICONDEST = /usr/share/icons/hicolor
DESKDEST = /usr/share/applications

APPS = apps
PNG = streamerretriever.png
SVG = streamerretriever.svg
PY = streamerretriever-gui.py
DESKFILE = streamerretriever_gui.desktop
SCALABLE = scalable/$(APPS)
X512 = 512x512/$(APPS)
X256 = 256x256/$(APPS)
X128 = 128x128/$(APPS)
X64 = 64x64/$(APPS)
X48 = 48x48/$(APPS)
X32 = 32x32/$(APPS)
X24 = 24x24/$(APPS)
X22 = 22x22/$(APPS)
X16 = 16x16/$(APPS)

FILES = CHANGELOG.md LICENSE README.md makefile
ALLFILES = $(SRC) $(REQ) $(ICONS) $(FILES) $(RES)/$(DESKFILE) $(PY) $(RES)/rc_icons.qrc $(THEMES)

VER := $$(sed -n "/__version__/p" $(SRC)/constants.py | cut -d"'" -f2)
OLD256 := $$(sed -n "/sha256sums/p" $(AUR)/PKGBUILD | cut -d'"' -f2)
SHA256 := $$(sha256sum "bin/tar/$(NAME)-$(VER).tar.gz" | cut -d" " -f1)

PYTHON = python3
.PHONY = clean test run version tarball install pkgbuild


version:
	@echo $(VER)

generate-py:
	uic -g python $(RES)/ui/ui_about.ui -o $(SRC)/ui_about.py
	uic -g python $(RES)/ui/ui_changelog.ui -o $(SRC)/ui_changelog.py
	# uic -g python $(RES)/ui/ui_chooser.ui -o $(SRC)/ui_chooser.py
	# sed -i "/^from PySide2.QtWidgets import \*/a from . import tools_gui" $(SRC)/ui_chooser.py
	# sed -i "s/self.tableWidget = QTableWidget(Chooser)/self.tableWidget = tools_gui\.DragColumns(Chooser)/g" $(SRC)/ui_chooser.py
	uic -g python $(RES)/ui/ui_followedit.ui -o $(SRC)/ui_followedit.py
	uic -g python $(RES)/ui/ui_maingui.ui -o $(SRC)/ui_maingui.py
	uic -g python $(RES)/ui/ui_twitchtoken.ui -o $(SRC)/ui_twitchtoken.py
	sed -i "s/import rc_icons_rc/from . import ui_maingui/g" $(SRC)/ui_maingui.py
	uic -g python $(RES)/ui/ui_settingsedit.ui -o $(SRC)/ui_settingsedit.py
	# uic -g python $(RES)/ui/ui_tableview.ui -o $(SRC)/ui_tableview.py

generate-rc:
	rcc -g python $(RES)/rc_icons.qrc -o $(SRC)/rc_icons.py

generate-png:
	inkscape --export-type="png" -h 512 -w 512 $(ICONS)/$(SVG) --export-filename=$(ICONS)/png/$(X512)/$(PNG)
	inkscape --export-type="png" -h 256 -w 256 $(ICONS)/$(SVG) --export-filename=$(ICONS)/png/$(X256)/$(PNG)
	inkscape --export-type="png" -h 128 -w 128 $(ICONS)/$(SVG) --export-filename=$(ICONS)/png/$(X128)/$(PNG)
	inkscape --export-type="png" -h 64 -w 64 $(ICONS)/$(SVG) --export-filename=$(ICONS)/png/$(X64)/$(PNG)
	inkscape --export-type="png" -h 48 -w 48 $(ICONS)/$(SVG) --export-filename=$(ICONS)/png/$(X48)/$(PNG)
	inkscape --export-type="png" -h 32 -w 32 $(ICONS)/$(SVG) --export-filename=$(ICONS)/png/$(X32)/$(PNG)
	inkscape --export-type="png" -h 24 -w 24 $(ICONS)/$(SVG) --export-filename=$(ICONS)/png/$(X24)/$(PNG)
	inkscape --export-type="png" -h 22 -w 22 $(ICONS)/$(SVG) --export-filename=$(ICONS)/png/$(X22)/$(PNG)
	inkscape --export-type="png" -h 16 -w 16 $(ICONS)/$(SVG) --export-filename=$(ICONS)/png/$(X16)/$(PNG)

clean:
	find . -name '*.pyc' -exec rm --force {} +
	find . -name '*.pyo' -exec rm --force {} +
	rm --force --recursive $(SRC)/__pycache__
	rm --force --recursive $(REQ)/__pycache__

test:
	# pipenv check
	@printf "\nflake8\n"
	@$(PYTHON) -m flake8 --exclude="ui_*, rc_*" $(SRC) && printf "OK\n\n"

run:
	@$(PYTHON) -m $(SRC).maingui

pkgbuild:
	@sed -i "s/^pkgver=.*/pkgver=$(VER)/g" $(AUR)/PKGBUILD
	@sed -i "s/$(OLD256)/$(SHA256)/g" $(AUR)/PKGBUILD

tarball: test clean
	@printf "\n"
	tar -czvf bin/tar/$(NAME)-$(VER).tar.gz --transform 's,^,streamerretriever-gui/,' $(ALLFILES)

install:
	install -d $(DESTDIR)$(PREFIX)/$(SRC)/$(SRC)
	install -m 755 $(PY) $(DESTDIR)$(PREFIX)/$(SRC)
	install -m 644 $(FILES) $(DESTDIR)$(PREFIX)/$(SRC)
	install -m 644 $(SRC)/* $(DESTDIR)$(PREFIX)/$(SRC)/$(SRC)
	install -d $(DESTDIR)$(PREFIX)/$(SRC)/$(REQ)
	install -m 644 $(REQ)/* $(DESTDIR)$(PREFIX)/$(SRC)/$(REQ)
	install -d $(DESTDIR)$(PREFIX)/$(SRC)/$(THEMES)
	install -m 644 $(THEMES)/* $(DESTDIR)$(PREFIX)/$(SRC)/$(THEMES)
	install -d $(DESTDIR)$(DESKDEST)
	install -m 644 $(RES)/$(DESKFILE) $(DESTDIR)$(DESKDEST)
	install -d $(DESTDIR)$(ICONDEST)/$(SCALABLE)
	install -m 644 $(ICONS)/$(SVG) $(DESTDIR)$(ICONDEST)/$(SCALABLE)
	install -d $(DESTDIR)$(ICONDEST)/$(X512)
	install -m 644 $(ICONS)/png/$(X512)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X512)
	install -d $(DESTDIR)$(ICONDEST)/$(X256)
	install -m 644 $(ICONS)/png/$(X256)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X256)
	install -d $(DESTDIR)$(ICONDEST)/$(X128)
	install -m 644 $(ICONS)/png/$(X128)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X128)
	install -d $(DESTDIR)$(ICONDEST)/$(X64)
	install -m 644 $(ICONS)/png/$(X64)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X64)
	install -d $(DESTDIR)$(ICONDEST)/$(X48)
	install -m 644 $(ICONS)/png/$(X48)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X48)
	install -d $(DESTDIR)$(ICONDEST)/$(X32)
	install -m 644 $(ICONS)/png/$(X32)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X32)
	install -d $(DESTDIR)$(ICONDEST)/$(X24)
	install -m 644 $(ICONS)/png/$(X24)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X24)
	install -d $(DESTDIR)$(ICONDEST)/$(X22)
	install -m 644 $(ICONS)/png/$(X22)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X22)
	install -d $(DESTDIR)$(ICONDEST)/$(X16)
	install -m 644 $(ICONS)/png/$(X16)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X16)

uninstall:
	@printf "# Remove folder\n"
	rm -rI $(DESTDIR)$(PREFIX)/$(SRC)
	@printf "\n# Remove icons\n"
	rm -I $(DESTDIR)$(ICONDEST)/$(SCALABLE)/$(SVG) $(DESTDIR)$(ICONDEST)/$(X512)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X256)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X128)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X64)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X48)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X32)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X24)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X22)/$(PNG) $(DESTDIR)$(ICONDEST)/$(X16)/$(PNG)
	@printf "\n# Remove desktop file\n"
	rm -i $(DESTDIR)$(DESKDEST)/$(DESKFILE)
