#! /usr/bin/env python3


import logging

from PySide2.QtWidgets import (
    QTableWidget, QAbstractItemView, QVBoxLayout, QDialogButtonBox)
from PySide2.QtCore import QCoreApplication, QMetaObject
from PySide2.QtGui import QDropEvent

from . import constants


logger = logging.getLogger(__name__)
logger.setLevel(constants.LOG_LEVEL)

file_handler = logging.FileHandler(constants.GUI_TOOLS_LOG)
formatter = logging.Formatter(constants.GUI_TOOLS_FORM)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class DragColumns(QTableWidget):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.viewport().setAcceptDrops(True)
        self.setDragDropOverwriteMode(False)
        self.setDropIndicatorShown(True)

        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.setDragDropMode(QAbstractItemView.InternalMove)

    #############################
    # Switch Drop-Selected places
    # def dropEvent(self, event: QDropEvent):
    #     if not event.isAccepted() and event.source() == self:
    #         s = self.selectedItems()[0]
    #         selected = (s.row(), s.column())
    #         dropped = self.dropOn(event)

    #         s_data = self.item(selected[0], selected[1]).text()
    #         d_data = self.item(dropped[0], dropped[1]).text()

    #         self.item(selected[0], selected[1]).setText(d_data)
    #         self.item(dropped[0], dropped[1]).setText(s_data)

    #         self.setCurrentCell(dropped[0], dropped[1])

    def dropEvent(self, event: QDropEvent):
        if not event.isAccepted() and event.source() == self:
            s = self.selectedItems()[0]
            selected = (s.row(), s.column())
            dropped = self.dropOn(event)

            columns = self.columnCount()
            items = [self.item(selected[0], i).text() for i in range(columns)]

            sel = items.pop(selected[1])
            items.insert(dropped[1], sel)

            y = selected[0]
            for x in range(columns):
                self.item(y, x).setText(items[x])

            self.setCurrentCell(dropped[0], dropped[1])

            # s_data = self.item(selected[0], selected[1]).text()
            # d_data = self.item(dropped[0], dropped[1]).text()

            # self.item(selected[0], selected[1]).setText(d_data)

            # if selected[1] < dropped[1]:
            #     start = selected
            #     end = dropped
            # elif dropped[1] < selected[1]:
            #     start = dropped
            #     end = selected

            # # temp = self.item(end[0], end[1]).text()

            # y = start[0]
            # x = start[1]
            # for i in range(abs(start[1] - end[1])):
            #     temp1 = self.item(y, x).text()
            #     temp2 = self.item(y, x+1).text()

            #     self.item(y, x).setText(temp2)

            #     print(str(x) + " " + temp)
            #     self.item(y, x).setText(temp)

    def dropOn(self, event):
        index = self.indexAt(event.pos())

        if not index.isValid():
            return self.columnCount()
        else:
            return (index.row(), index.column())


class Ui_Chooser(object):
    def __init__(self, table):
        self.__table = table

    def setupUi(self, Chooser):
        if not Chooser.objectName():
            Chooser.setObjectName(u"Chooser")
        Chooser.resize(480, 320)
        self.verticalLayout = QVBoxLayout(Chooser)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.tableWidget = self.__table(Chooser)
        self.tableWidget.setObjectName(u"tableWidget")

        self.verticalLayout.addWidget(self.tableWidget)

        self.buttonBox = QDialogButtonBox(Chooser)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setStandardButtons(
            QDialogButtonBox.Cancel | QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Chooser)

        QMetaObject.connectSlotsByName(Chooser)

    def retranslateUi(self, Chooser):
        Chooser.setWindowTitle(QCoreApplication.translate(
            "Chooser", u"Streamchecker - Chooser", None))
