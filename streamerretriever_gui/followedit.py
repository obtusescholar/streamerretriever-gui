#! /usr/bin/env python3


import os
import sys
import logging

from PySide2.QtWidgets import (
    QStyledItemDelegate,
    QAbstractItemView,
    QDialogButtonBox,
    QSystemTrayIcon,
    QApplication,
    QInputDialog,
    QHeaderView,
    QDialog,
    QStyle)
from PySide2.QtCore import (
    QAbstractTableModel,
    QPoint,
    QRect,
    QSize,
    Qt)
from PySide2.QtGui import (
    QPixmap,
    QIcon)

from . import constants
from . ui_followedit import Ui_FollowEdit
from . twitchload import TwitchLoad


logger = logging.getLogger(__name__)
logger.setLevel(constants.LOG_LEVEL)

file_handler = logging.FileHandler(constants.FOLLOWS_LOG)
formatter = logging.Formatter(constants.FOLLOWS_FORM)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class TableModel(QAbstractTableModel):

    def __init__(self, data, table, icons, labels, icon_size):
        super().__init__()
        self.__data = data
        self.__orig = data
        self.__table = table
        self.__icons = icons
        self.__labels = labels
        self.__icon_size = icon_size

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            return self.__labels[section]

        return QAbstractTableModel.headerData(self, section, orientation, role)

    def data(self, index, role):
        if role == Qt.DisplayRole:
            return self.__data[index.row()][index.column()]

        # if role == Qt.DecorationRole:
        #     value = self.__data[index.row()][index.column()]
        #     if isinstance(value, QPixmap):
        #         # label = QLabel(value)
        #         # return label
        #         return value
        #         # return QIcon(value)
        #         # return QPixmap(value)

        # if role == Qt.TextAlignmentRole:
        #     value = self.__data[index.row()][index.column()]
        #     if isinstance(value, QIcon):
        #         return Qt.AlignCenter

    def getNames(self):
        for data in self.__data:
            yield data[1]

    def rowCount(self, index):
        return len(self.__data)

    def columnCount(self, index):
        if self.__data:
            return len(self.__data[0])
        else:
            return 0

    def sort(self, column, order=Qt.AscendingOrder):
        self.layoutAboutToBeChanged.emit()
        rows = range(len(self.__data))

        try:
            sort_list = [float(self.__data[row][column]) for row in rows]
        except ValueError:
            try:
                sort_list = [self.__data[row][column].lower() for row in rows]
            except AttributeError:
                sort_list = [self.__data[row][column] for row in rows]

        if order == Qt.AscendingOrder:
            new = [x for _, x in sorted(zip(sort_list, self.__data))]
        elif order == Qt.DescendingOrder:
            new = [x for _, x in sorted(
                zip(sort_list, self.__data), reverse=True)]

        self.__data = new

        if self.__icons:
            delegate = MyDelegate(
                self.__data, self.__icons, self.__icon_size)
            self.__table.setItemDelegateForColumn(0, delegate)
        self.layoutChanged.emit()

    def restoreSorting(self):
        self.layoutAboutToBeChanged.emit()
        self.__data = list(self.__orig)

        if self.__icons:
            delegate = MyDelegate(
                self.__data, self.__icons, self.__icon_size)
            self.__table.setItemDelegateForColumn(0, delegate)
        self.layoutChanged.emit()

    def moveUp(self, index):
        if index.row() == 0:
            return
        else:
            self.layoutAboutToBeChanged.emit()
            up = self.__data.pop(index.row())
            self.__data.insert(index.row() - 1, up)
            self.layoutChanged.emit()

    def moveDown(self, index):
        if index.row() == self.rowCount(index) - 1:
            return
        else:
            self.layoutAboutToBeChanged.emit()
            down = self.__data.pop(index.row())
            self.__data.insert(index.row() + 1, down)
            self.layoutChanged.emit()


# class IconCenterDelegate(QStyledItemDelegate):
#     def __init__(self, icons, parent=None):
#         super().__init__()
#         self._icons = icons
#
#     def getIcon(self, index):
#         icon = self._icons[index.row() % len(self._icons)]
#         return icon
#
#     def paint(self, painter, option, index):
#         icon = self.getIcon(index)
#         icon.paint(painter, option.rect, Qt.AlignCenter)

    # def initStyleOption(self, option, index):
    #     super(IconCenterDelegate, self).initStyleOption(option, index)
    #     option.decorationAlignment = (
    #         Qt.AlignHCenter | Qt.AlignVCenter)
    #     # option.decorationPosition = QStyleOptionViewItem.Top
    #     return option


class MyDelegate(QStyledItemDelegate):
    def __init__(self, database, icons, icon_size, parent=None):
        super().__init__(parent)
        self.__database = database
        self.__icons = icons
        self.__parent = parent
        self.__icon_size = icon_size

    def getIcon(self, index):
        # icon = self.__icons[index.row() % len(self.__icons)]
        # user_id =
        #     self.__database[index.row() % len(self.__icons)].getUserId()
        user_id = self.__database[index.row() % len(self.__icons)][3]
        # icon = QIcon(self.__icons[user_id])
        icon = self.__icons[user_id] or None
        return icon

    # def sizeHint(self, option, index):
    #     # return option.rect.setSize(QSize(10, 10))
    #     # option.rect.setSize(QSize(10, 10))
    #     return QSize(10, 10)

    def paint(self, painter, option, index):
        # painter.save()
        # item = self.__parent.item(index)
        # widget = self.__parent.tableViewDatabase.itemWidget(item)
        # pixmap = QPixmap(self.getIcon(index))
        # painter.restore()

        # y = option.rect.height()
        # i = index.row()
        # cy = (y / 2) - 15 + (i * y)

        # x = option.rect.width()
        # cx = (x / 2) - 15

        # if option.state and QStyle.State_HasFocus:
        #     painter = fillRect(option.rect, option.palette.highlight())

        if QStyle.State_Item:
            super().paint(painter, option, index)
        else:
            super().paint(painter, option, index)

        icon = QIcon(self.getIcon(index))
        size = QSize(self.__icon_size, self.__icon_size)
        size_h = size.height()
        size_w = size.width()

        # option.rect.moveCenter(option.rect.center())
        # option.rect.moveLeft(c_w)
        # option.rect.setSize(QSize(30, 30))
        # print(option.rect)
        h = option.rect.height()
        w = option.rect.width()
        x = option.rect.x() + w/2 - size_w/2
        y = option.rect.y() + h/2 - size_h/2
        # print(str(h) + ' ' + str(w))
        rect = QRect(x, y, size_w, size_h)
        # option.rect.moveTo(QPoint(cx, cy))
        # option.decorationAlignment = Qt.AlignCenter
        icon.paint(painter, rect, Qt.AlignCenter, mode=QIcon.Normal)

        # ch = h / 2
        # cw = w / 2
        # print(str(ch) + ' ' + str(cw))

        # option.rect.setHeight(10)
        # option.rect.setWidth(10)
        # option.decorationAlignment = Qt.AlignCenter

        # pix = QPixmap(icon)
        # option.rect.setSize(QSize(40, 40))
        # painter.drawPixmap(option.rect, pix)

    # def paint(self, painter, option, index):
    #     icon = self.getIcon(index)
    #     return QIcon(icon)


class FollowEdit(QDialog):

    def __init__(self, *ignore, parent=None, api):
        super().__init__(parent)
        self.__api = api
        self.__parent = parent
        self.__conf = self.__api.getConfigs()
        self.__follows = list(self.__api.getFollows())

        self.ui = Ui_FollowEdit()

        self.ui.setupUi(self)
        self.__buttonIcons()
        self.__followsPopulate()
        self.__followsButtons()
        self.__databasePopulate()
        self.setWindowFlag(
            Qt.FramelessWindowHint, self.__conf.showTitlebar)
        self.ui.listWidgetFollow.setSpacing(0)

        # self.ui.pushButtonFind.setEnabled(False)

        self.show()

    def __followsPopulate(self):
        """Add follows into follows list."""

        self.ui.listWidgetFollow.clear()
        for i, follow in enumerate(self.__follows):
            self.ui.listWidgetFollow.addItem(follow)

    def __followsButtons(self):
        self.ui.pushButtonAdd.clicked.connect(self.__followsAdd)
        self.ui.pushButtonEdit.clicked.connect(self.__followsEdit)
        self.ui.pushButtonDelete.clicked.connect(self.__followsDelete)
        self.ui.pushButtonLoadTwitch.clicked.connect(self.__showLoadTwitch)

        self.ui.pushButtonUp.clicked.connect(self.__moveUp)
        self.ui.pushButtonDown.clicked.connect(self.__moveDown)
        self.ui.pushButtonRestoreSorting.clicked.connect(self.__restoreSorting)

        self.ui.buttonBox.clicked.connect(self.__handleButtons)

    def __reloadData(self):
        self.__follows = list(self.__api.getFollows())
        self.__databasePopulate()
        self.__followsPopulate()

    def __handleButtons(self, button):
        sb = self.ui.buttonBox.standardButton(button)
        if sb == QDialogButtonBox.Apply:
            self.__saveChanges()
            self.__reloadData()
            # self.__follows = list(self.__api.getFollows())
            # self.__databasePopulate()
            # self.__followsPopulate()
        elif sb == QDialogButtonBox.Ok:
            self.__saveChanges()

    def __saveChanges(self):
        db_names = list(self.model.getNames())
        list_widget = [
            j.text() for j in
            self.ui.listWidgetFollow.findItems('*', Qt.MatchWildcard)]

        wo_deleted = [n for n in db_names if n in list_widget]
        new = [m for m in list_widget if m not in db_names]

        new.extend(wo_deleted)
        self.__api.updateFollows(new, self.__parent.redrawTable)

        not_found = self.__api.getNotFound()
        if not_found:
            self.__parent.tray.showMessage(
                'Name not found\n', ' '.join(not_found),
                QSystemTrayIcon.Warning, 4000)

    # def __notFound(self):
    #     new_db = [d.getName() for d in self.__api.getDb()]
    #     not_found = [a for a in self.__follows if a not in new_db]

    def __followsAdd(self):
        """Add name into list from line-edit. Regex and lower it."""

        txt = (self.ui.lineEditFollow.text().lower(), )
        logger.debug(f'Add to List: "{txt[0]}"')

        value = self.__api.regexFollows(txt)
        if value and value[0] not in self.__follows:
            self.__follows.insert(0, value[0])
            self.ui.listWidgetFollow.insertItem(0, value[0])
            self.ui.lineEditFollow.setText('')
            logger.info(f'Add to List: Name "{value}" added')
        else:
            self.ui.lineEditFollow.setText('')
            logger.info(f'Add to List: Name "{value}" already in list')

    def __followsDelete(self):
        """Delete name from self.__follows and from list."""

        selected = self.ui.listWidgetFollow.currentRow()
        if selected == -1:
            return

        logger.debug(f'Delete from List: "{selected}"')

        self.__follows.remove(self.__follows[selected])
        self.ui.listWidgetFollow.takeItem(selected)

    def __followsEdit(self):
        """Open dialog to edit name from list."""

        selected = self.ui.listWidgetFollow.currentRow()
        if selected == -1:
            return

        txt = self.ui.listWidgetFollow.item(selected).text()
        logger.debug(f'Edit from List: "{txt}"')

        newname, ok = QInputDialog.getText(
            self, 'Enter new name', 'Enter new name', text=txt)
        if ok:
            logger.debug(f'Edit into List: "{newname}"')
            if len(newname) > 0:
                self.__follows[selected] = newname
                self.ui.listWidgetFollow.takeItem(selected)
                self.ui.listWidgetFollow.insertItem(selected, newname)

    def __buttonIcons(self):
        t = self.__conf.iconTheme
        plus = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/plus.svg'))
        self.ui.pushButtonAdd.setIcon(plus)

        pen = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/pen.svg'))
        self.ui.pushButtonEdit.setIcon(pen)

        minus = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/minus.svg'))
        self.ui.pushButtonDelete.setIcon(minus)

        download = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/download.svg'))
        self.ui.pushButtonLoadTwitch.setIcon(download)

        up = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/up.svg'))
        self.ui.pushButtonUp.setIcon(up)

        down = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/down.svg'))
        self.ui.pushButtonDown.setIcon(down)

        restore = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/restore.svg'))
        self.ui.pushButtonRestoreSorting.setIcon(restore)

    def __moveUp(self):
        selected = self.ui.tableViewDatabase.selectionModel().selectedRows()
        if selected and selected[0]:
            self.model.moveUp(selected[0])
            self.ui.tableViewDatabase.selectRow(selected[0].row() - 1)

    def __moveDown(self):
        selected = self.ui.tableViewDatabase.selectionModel().selectedRows()
        if selected and selected[0]:
            self.model.moveDown(selected[0])
            self.ui.tableViewDatabase.selectRow(selected[0].row() + 1)

    def __restoreSorting(self):
        self.model.restoreSorting()

    def __databasePopulate(self):
        database = self.__api.getDb()
        data = []

        keys = self.__api.getKeysSource(['userdata'])
        labels = [
            keys['name']['pretty'],
            keys['display_name']['pretty'],
            keys['user_id']['pretty'],
            keys['description']['pretty'],
            keys['partner']['pretty'],
            keys['view_count']['pretty']]

        row_height = self.__conf.rowHeight

        self.ui.tableViewDatabase.setSelectionMode(
            QAbstractItemView.SingleSelection)
        self.ui.tableViewDatabase.setSelectionBehavior(
            QAbstractItemView.SelectRows)

        if self.__conf.enableIcons is True:
            icons = self.__api.getIcons()
            icon_size = row_height - self.__conf.marginRow
            # self.ui.tableViewDatabase.setIconSize(
            #     QSize(icon_size, icon_size))
            vertical = self.ui.tableViewDatabase.verticalHeader()
            vertical.setDefaultSectionSize(row_height * 2)
            labels.insert(0, 'Icon')
            # vertical.setSectionsMovable(True)
            # vertical.setDragEnabled(True)
            # vertical.setDragDropMode(QAbstractItemView.InternalMove)
        else:
            icon_size = None
            icons = False

        for i, d in enumerate(database):
            # icon = icons[d.getUserId()]
            # pixmap = QPixmap(icon)
            # pixmap = pixmap.scaledToHeight(
            #     icon_size, mode=Qt.SmoothTransformation)
            data.append([
                # pixmap,
                d.getName(),
                d.getDispName(),
                d.getUserId(),
                d.getDesc(),
                d.getPartner(),
                d.getViewCount()])

            if self.__conf.enableIcons:
                data[i].insert(0, '')

        header = self.ui.tableViewDatabase.horizontalHeader()
        header.setSectionsMovable(True)
        header.setDragEnabled(True)
        header.setDragDropMode(QAbstractItemView.InternalMove)

        header.setSectionResizeMode(QHeaderView.ResizeToContents)
        self.ui.tableViewDatabase.resizeColumnsToContents()

        self.model = TableModel(
            data, self.ui.tableViewDatabase, icons, labels, icon_size)
        self.ui.tableViewDatabase.setModel(self.model)
        # centerDelegate = IconCenterDelegate(self.ui.tableViewDatabase)
        # self.ui.tableViewDatabase.setItemDelegateForColumn(0, centerDelegate)

        self.ui.tableViewDatabase.setHorizontalScrollMode(
            QAbstractItemView.ScrollPerPixel)
        self.ui.tableViewDatabase.setWordWrap(True)
        # self.ui.tableViewDatabase.setWordWrap(False)

        if self.__conf.enableIcons:
            index = 4
        else:
            index = 3

        if data:
            header.setSectionResizeMode(index, QHeaderView.Stretch)
        header.setVisible(True)
        # header.setSectionResizeMode(0, QHeaderView.Fixed)

        # if icons:
        #     self.ui.tableViewDatabase.setColumnWidth(0, icon_size)

        if self.__conf.showGrid and not self.ui.tableViewDatabase.showGrid():
            self.ui.tableViewDatabase.setShowGrid(True)
        elif not self.__conf.showGrid and self.ui.tableViewDatabase.showGrid():
            self.ui.tableViewDatabase.setShowGrid(False)

        # delegate = MyDelegate(
        #     database, icons, self.ui.tableViewDatabase)
        # self.ui.tableViewDatabase.setItemDelegateForColumn(0, delegate)

        self.ui.tableViewDatabase.setSortingEnabled(True)
        self.model.restoreSorting()
        # self.ui.tableViewDatabase.sortByColumn(0, Qt.AscendingOrder)

    def __showLoadTwitch(self):
        twitch_follows = self.__api.getTwitchFollows()
        twitch_load = TwitchLoad(
            self.__api,
            twitch_follows,
            self.__follows,
            twitch_follows,
            parent=self.__parent)
        if twitch_load.exec_():
            additions = list(self.__parent.chooserList)
            additions.extend(self.__follows)
            self.__api.updateFollows(additions)
            self.__reloadData()

    def mousePressEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            self.oldPos = event.globalPos()
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            delta = QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()
        else:
            super().mouseMoveEvent(event)


def main():
    app = QApplication([])
    widget = FollowEdit()
    widget.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
