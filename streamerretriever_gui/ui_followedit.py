# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_followedit.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_FollowEdit(object):
    def setupUi(self, FollowEdit):
        if not FollowEdit.objectName():
            FollowEdit.setObjectName(u"FollowEdit")
        FollowEdit.resize(1080, 800)
        self.verticalLayout_3 = QVBoxLayout(FollowEdit)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.pushButtonAdd = QPushButton(FollowEdit)
        self.pushButtonAdd.setObjectName(u"pushButtonAdd")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.pushButtonAdd.sizePolicy().hasHeightForWidth())
        self.pushButtonAdd.setSizePolicy(sizePolicy)
        self.pushButtonAdd.setMinimumSize(QSize(40, 40))

        self.verticalLayout_2.addWidget(self.pushButtonAdd)

        self.pushButtonEdit = QPushButton(FollowEdit)
        self.pushButtonEdit.setObjectName(u"pushButtonEdit")
        sizePolicy.setHeightForWidth(self.pushButtonEdit.sizePolicy().hasHeightForWidth())
        self.pushButtonEdit.setSizePolicy(sizePolicy)
        self.pushButtonEdit.setMinimumSize(QSize(40, 40))

        self.verticalLayout_2.addWidget(self.pushButtonEdit)

        self.pushButtonDelete = QPushButton(FollowEdit)
        self.pushButtonDelete.setObjectName(u"pushButtonDelete")
        sizePolicy.setHeightForWidth(self.pushButtonDelete.sizePolicy().hasHeightForWidth())
        self.pushButtonDelete.setSizePolicy(sizePolicy)
        self.pushButtonDelete.setMinimumSize(QSize(40, 40))

        self.verticalLayout_2.addWidget(self.pushButtonDelete)

        self.pushButtonLoadTwitch = QPushButton(FollowEdit)
        self.pushButtonLoadTwitch.setObjectName(u"pushButtonLoadTwitch")
        sizePolicy.setHeightForWidth(self.pushButtonLoadTwitch.sizePolicy().hasHeightForWidth())
        self.pushButtonLoadTwitch.setSizePolicy(sizePolicy)
        self.pushButtonLoadTwitch.setMinimumSize(QSize(40, 40))

        self.verticalLayout_2.addWidget(self.pushButtonLoadTwitch)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer)


        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.lineEditFollow = QLineEdit(FollowEdit)
        self.lineEditFollow.setObjectName(u"lineEditFollow")

        self.verticalLayout.addWidget(self.lineEditFollow)

        self.listWidgetFollow = QListWidget(FollowEdit)
        self.listWidgetFollow.setObjectName(u"listWidgetFollow")
        self.listWidgetFollow.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        self.listWidgetFollow.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)

        self.verticalLayout.addWidget(self.listWidgetFollow)


        self.horizontalLayout.addLayout(self.verticalLayout)


        self.verticalLayout_3.addLayout(self.horizontalLayout)

        self.verticalSpacer_3 = QSpacerItem(20, 15, QSizePolicy.Minimum, QSizePolicy.Fixed)

        self.verticalLayout_3.addItem(self.verticalSpacer_3)

        self.labelDatabase = QLabel(FollowEdit)
        self.labelDatabase.setObjectName(u"labelDatabase")

        self.verticalLayout_3.addWidget(self.labelDatabase)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.pushButtonUp = QPushButton(FollowEdit)
        self.pushButtonUp.setObjectName(u"pushButtonUp")
        sizePolicy.setHeightForWidth(self.pushButtonUp.sizePolicy().hasHeightForWidth())
        self.pushButtonUp.setSizePolicy(sizePolicy)
        self.pushButtonUp.setMinimumSize(QSize(40, 40))

        self.gridLayout.addWidget(self.pushButtonUp, 0, 0, 1, 1)

        self.tableViewDatabase = QTableView(FollowEdit)
        self.tableViewDatabase.setObjectName(u"tableViewDatabase")
        self.tableViewDatabase.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)

        self.gridLayout.addWidget(self.tableViewDatabase, 0, 1, 4, 1)

        self.pushButtonDown = QPushButton(FollowEdit)
        self.pushButtonDown.setObjectName(u"pushButtonDown")
        sizePolicy.setHeightForWidth(self.pushButtonDown.sizePolicy().hasHeightForWidth())
        self.pushButtonDown.setSizePolicy(sizePolicy)
        self.pushButtonDown.setMinimumSize(QSize(40, 40))

        self.gridLayout.addWidget(self.pushButtonDown, 1, 0, 1, 1)

        self.pushButtonRestoreSorting = QPushButton(FollowEdit)
        self.pushButtonRestoreSorting.setObjectName(u"pushButtonRestoreSorting")
        sizePolicy.setHeightForWidth(self.pushButtonRestoreSorting.sizePolicy().hasHeightForWidth())
        self.pushButtonRestoreSorting.setSizePolicy(sizePolicy)
        self.pushButtonRestoreSorting.setMinimumSize(QSize(40, 40))

        self.gridLayout.addWidget(self.pushButtonRestoreSorting, 2, 0, 1, 1)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout.addItem(self.verticalSpacer_2, 3, 0, 1, 1)


        self.verticalLayout_3.addLayout(self.gridLayout)

        self.buttonBox = QDialogButtonBox(FollowEdit)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setMinimumSize(QSize(111, 28))
        self.buttonBox.setStandardButtons(QDialogButtonBox.Apply|QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout_3.addWidget(self.buttonBox)

        self.verticalLayout_3.setStretch(0, 1)
        self.verticalLayout_3.setStretch(3, 2)

        self.retranslateUi(FollowEdit)
        self.buttonBox.accepted.connect(FollowEdit.accept)
        self.buttonBox.rejected.connect(FollowEdit.reject)

        QMetaObject.connectSlotsByName(FollowEdit)
    # setupUi

    def retranslateUi(self, FollowEdit):
        FollowEdit.setWindowTitle(QCoreApplication.translate("FollowEdit", u"Streamer Retriever - Follows", None))
#if QT_CONFIG(tooltip)
        self.pushButtonAdd.setToolTip(QCoreApplication.translate("FollowEdit", u"<html><head/><body><p>Add name</p><p>&gt;Alt+A&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.pushButtonAdd.setShortcut(QCoreApplication.translate("FollowEdit", u"Alt+A", None))
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        self.pushButtonEdit.setToolTip(QCoreApplication.translate("FollowEdit", u"<html><head/><body><p>Edit name</p><p>&lt;Alt+E&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.pushButtonEdit.setShortcut(QCoreApplication.translate("FollowEdit", u"Alt+E", None))
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        self.pushButtonDelete.setToolTip(QCoreApplication.translate("FollowEdit", u"<html><head/><body><p>Remove name</p><p>&lt;Alt+D&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.pushButtonDelete.setShortcut(QCoreApplication.translate("FollowEdit", u"Alt+D", None))
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        self.pushButtonLoadTwitch.setToolTip(QCoreApplication.translate("FollowEdit", u"<html><head/><body><p>Load follows from Twitch.</p><p>&lt;Alt+T&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonLoadTwitch.setText("")
#if QT_CONFIG(shortcut)
        self.pushButtonLoadTwitch.setShortcut(QCoreApplication.translate("FollowEdit", u"Alt+T", None))
#endif // QT_CONFIG(shortcut)
        self.lineEditFollow.setPlaceholderText(QCoreApplication.translate("FollowEdit", u"Name or Link", None))
        self.labelDatabase.setText(QCoreApplication.translate("FollowEdit", u"<html><head/><body><p align=\"center\"><span style=\" font-size:12pt;\">Database</span></p></body></html>", None))
#if QT_CONFIG(tooltip)
        self.pushButtonUp.setToolTip(QCoreApplication.translate("FollowEdit", u"<html><head/><body><p>Move up</p><p>&lt;Alt+W&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.pushButtonUp.setShortcut(QCoreApplication.translate("FollowEdit", u"Alt+W", None))
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        self.pushButtonDown.setToolTip(QCoreApplication.translate("FollowEdit", u"<html><head/><body><p>Move down</p><p>&lt;Alt+S&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.pushButtonDown.setShortcut(QCoreApplication.translate("FollowEdit", u"Alt+S", None))
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        self.pushButtonRestoreSorting.setToolTip(QCoreApplication.translate("FollowEdit", u"<html><head/><body><p>Restore sorting</p><p>&lt;Alt+R&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.pushButtonRestoreSorting.setShortcut(QCoreApplication.translate("FollowEdit", u"Alt+R", None))
#endif // QT_CONFIG(shortcut)
    # retranslateUi

