#! /usr/bin/env python3

import os
import logging
from pathlib import Path

from . import constants
from streamerretriever.fileoperator import FileOperator


logger = logging.getLogger(__name__)
logger.setLevel(constants.LOG_LEVEL)

file_handler = logging.FileHandler(constants.CONFIG_LOG)
formatter = logging.Formatter(constants.CONFIG_FORM)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class Configs():
    _defaults = {
        'interval': 20,
        'show_toolbar': True,
        'notifications': True,
        'statusbar': False,
        'show_titlebar': False}
    _source = 'general'
    _conf_source = {'sources': ['twitch']}

    def __init__(self):
        base = constants.BASES[os.name]
        base_path = Path.resolve(Path.expanduser(Path(base)))

        config = constants.CONFIG
        config_path = base_path / config
        self.__config_fo = FileOperator(str(config_path))

        self.__file_configs = self.__config_fo.loadFile() or {}
        source_configs = self.__file_configs.get(self._source) or {}
        logger.debug(f'{source_configs}')

        self._configs = self._checkDefaults(source_configs)

    def _checkDefaults(self, configs):
        for key in self._defaults:
            if key not in configs:
                configs[key] = self._defaults[key]
        return configs

    # def getValue(self, key):
    #     return self._configs[key]

    def save(self):
        self.__file_configs[self._source] = self._configs
        self.__config_fo.saveFile(self.__file_configs)

    @property
    def sources(self):
        return self._conf_source['sources']

    @sources.setter
    def sources(self, value):
        self._configs['sources'] = value

    @property
    def interval(self):
        return self._configs['interval']

    @interval.setter
    def interval(self, value):
        self._configs['interval'] = value

    @property
    def showToolbar(self):
        return self._configs['show_toolbar']

    @showToolbar.setter
    def showToolbar(self, value):
        self._configs['show_toolbar'] = value

    @property
    def notifications(self):
        return self._configs['notifications']

    @notifications.setter
    def notifications(self, value):
        self._configs['notifications'] = value

    @property
    def statusbar(self):
        return self._configs['statusbar']

    @statusbar.setter
    def statusbar(self, value):
        self._configs['statusbar'] = value

    @property
    def showTitlebar(self):
        return self._configs['show_titlebar']

    @showTitlebar.setter
    def showTitlebar(self, value):
        self._configs['show_titlebar'] = value

    # def getHideToolbar(self):
    #     return self.getValue('show_toolbar')
    # def getInterval(self):
    #     return self.getValue('interval')


class TwitchConfigs(Configs):
    _general = {
        'window_size': [800, 600],
        'enable_icons': False,
        'enable_tray_icons': True,
        'show_toolbar_icons': False,
        'start_hidden': False,
        'show_side_panel': True,
        'use_mobile': False,
        'player_set': False,
        'player': [],
        'autostart': False,
        'auto_streams': [],
        'always_chat': False,
        'chat_set': False,
        'chat': []}

    _streams_table = {
        'icon_theme': 'stonewall-light',
        'theme': 'Fusion',
        'qss_default': True,
        'qss_enable': False,
        'qss_theme': None,
        'row_height': 35,
        # 'margin_offset': 0,
        # 'margin_col': 0,
        'margin_row': 4,
        'show_grid': False,
        'show_c_headers': True,
        'show_r_headers': False,
        'shown_columns': ['display_name', 'game', 'title', 'time'],
        'stretch_column': 'title',
        'font': False,
        'filter_auto': False}

    _source = 'twitch'

    def __init__(self):
        self._defaults.update(self._general)
        self._defaults.update(self._streams_table)
        super().__init__()

        # if self._configs['interval'] < 10:
        #     self._configs['interval'] = self._defaults['interval']

    #########################
    # General               #
    #########################
    @property
    def iconTheme(self):
        return self._configs['icon_theme']

    @iconTheme.setter
    def iconTheme(self, value):
        self._configs['icon_theme'] = value

    @property
    def theme(self):
        return self._configs['theme']

    @theme.setter
    def theme(self, value):
        self._configs['theme'] = value

    @property
    def qssDefault(self):
        return self._configs['qss_default']

    @qssDefault.setter
    def qssDefault(self, value):
        self._configs['qss_default'] = value

    @property
    def qssEnable(self):
        return self._configs['qss_enable']

    @qssEnable.setter
    def qssEnable(self, value):
        self._configs['qss_enable'] = value

    @property
    def qssTheme(self):
        return self._configs['qss_theme']

    @qssTheme.setter
    def qssTheme(self, value):
        self._configs['qss_theme'] = value

    @property
    def windowSizeX(self):
        return self._configs['window_size'][0]

    @windowSizeX.setter
    def windowSizeX(self, value):
        self._configs['window_size'][0] = value

    @property
    def windowSizeY(self):
        return self._configs['window_size'][1]

    @windowSizeY.setter
    def windowSizeY(self, value):
        self._configs['window_size'][1] = value

    @property
    def enableIcons(self):
        return self._configs['enable_icons']

    @enableIcons.setter
    def enableIcons(self, value):
        self._configs['enable_icons'] = value

    @property
    def enableTrayIcons(self):
        return self._configs['enable_tray_icons']

    @enableTrayIcons.setter
    def enableTrayIcons(self, value):
        self._configs['enable_tray_icons'] = value

    @property
    def showToolbarIcons(self):
        return self._configs['show_toolbar_icons']

    @showToolbarIcons.setter
    def showToolbarIcons(self, value):
        self._configs['show_toolbar_icons'] = value

    @property
    def startHidden(self):
        return self._configs['start_hidden']

    @startHidden.setter
    def startHidden(self, value):
        self._configs['start_hidden'] = value

    @property
    def showSidePanel(self):
        return self._configs['show_side_panel']

    @showSidePanel.setter
    def showSidePanel(self, value):
        self._configs['show_side_panel'] = value

    @property
    def useMobile(self):
        return self._configs['use_mobile']

    @useMobile.setter
    def useMobile(self, value):
        self._configs['use_mobile'] = value

    @property
    def playerSet(self):
        return self._configs['player_set']

    @playerSet.setter
    def playerSet(self, value):
        self._configs['player_set'] = value

    @property
    def player(self):
        return self._configs['player']

    @player.setter
    def player(self, value):
        self._configs['player'] = value

    @property
    def autostart(self):
        return self._configs['autostart']

    @autostart.setter
    def autostart(self, value):
        self._configs['autostart'] = value

    @property
    def autoStreams(self):
        return self._configs['auto_streams']

    @autoStreams.setter
    def autoStreams(self, value):
        self._configs['auto_streams'] = value

    @property
    def alwaysChat(self):
        return self._configs['always_chat']

    @alwaysChat.setter
    def alwaysChat(self, value):
        self._configs['always_chat'] = value

    @property
    def chatSet(self):
        return self._configs['chat_set']

    @chatSet.setter
    def chatSet(self, value):
        self._configs['chat_set'] = value

    @property
    def chat(self):
        return self._configs['chat']

    @chat.setter
    def chat(self, value):
        self._configs['chat'] = value

    #########################
    # Streams Table         #
    #########################
    @property
    def rowHeight(self):
        return self._configs['row_height']

    @rowHeight.setter
    def rowHeight(self, value):
        self._configs['row_height'] = value

    # @property
    # def marginOffset(self):
    #     return self._configs['margin_offset']

    # @marginOffset.setter
    # def marginOffset(self, value):
    #     self._configs['margin_offset'] = value

    # @property
    # def marginCol(self):
    #     return self._configs['margin_col']

    # @marginCol.setter
    # def marginCol(self, value):
    #     self._configs['margin_col'] = value

    @property
    def marginRow(self):
        return self._configs['margin_row']

    @marginRow.setter
    def marginRow(self, value):
        self._configs['margin_row'] = value

    @property
    def showGrid(self):
        return self._configs['show_grid']

    @showGrid.setter
    def showGrid(self, value):
        self._configs['show_grid'] = value

    @property
    def showCHeaders(self):
        return self._configs['show_c_headers']

    @showCHeaders.setter
    def showCHeaders(self, value):
        self._configs['show_c_headers'] = value

    @property
    def showRHeaders(self):
        return self._configs['show_r_headers']

    @showRHeaders.setter
    def showRHeaders(self, value):
        self._configs['show_r_headers'] = value

    @property
    def shownColumns(self):
        return self._configs['shown_columns']

    @shownColumns.setter
    def shownColumns(self, value):
        self._configs['shown_columns'] = value

    @property
    def stretchColumn(self):
        return self._configs['stretch_column']

    @stretchColumn.setter
    def stretchColumn(self, value):
        self._configs['stretch_column'] = value

    @property
    def font(self):
        return self._configs['font']

    @font.setter
    def font(self, value):
        self._configs['font'] = value

    @property
    def filterAuto(self):
        return self._configs['filter_auto']

    @filterAuto.setter
    def filterAuto(self, value):
        self._configs['filter_auto'] = value

    # def getWindowSizeX(self):
    #     return self.getValue('window_size')[0]
    # def getWindowSizeY(self):
    #     return self.getValue('window_size')[1]
    # def getShowCHeader(self):
    #     return self.getValue('show_c_header')
    # def getShowRHeader(self):
    #     return self.getValue('show_r_header')
    # def getShowRHeader(self):
    #     return self.getValue('show_c_header')
    # def getShowGrid(self):
    #     return self.getValue('show_grid')
    # def getRowHeight(self):
    #     return self.getValue('row_height')
    # def getMarginRow(self):
    #     return self.getValue('margin_row')
    # def getMarginCol(self):
    #     return self.getValue('margin_col')
    # def getMarginOffset(self):
    #     return self.getValue('margin_offset')
    # def getShowColumns(self):
    #     return self.getValue('shown_columns')
    # def getFont(self):
    #     return self.getValue('font')
    # def getStretchColumn(self):
    #     return self.getValue('stretch_column')
    # def getStreamerIcons(self):
    #     return self.getValue('enable_icons')
    # def getPlayer(self):
    #     return self.getValue('player')
    # def getPlayerSet(self):
    #     return self.getValue('player_set')
    # def getChat(self):
    #     return self.getValue('chat')
    # def getChatSet(self):
    #     return self.getValue('chat_set')
    # def getMobile(self):
    #     return self.getValue('player_mobile')
    # def getAutostarting(self):
    #     return self.getValue('autostart')
    # def getAutoStreams(self):
    #     return self.getValue('auto_streams')
    # def getFilterAuto(self):
    #     return self.getValue('filter_auto')
    # def getAlwaysChat(self):
    #     return self.getValue('always_chat')
    # def getOpenHidden(self):
    #     return self.getValue('start_hidden')
    # def getInterval(self):
    #     if 'interval' in self._defaults:
    #         interval = self.getValue('interval')
    #     else:
    #         conf = Configs()
    #         interval = conf.getInterval()
    #     return interval
    # def getHideToolbar(self):
    #     if 'show_toolbar' in self._defaults:
    #         show_toolbar = self.getValue('show_toolbar')
    #     else:
    #         conf = Configs()
    #         show_toolbar = conf.getHideToolbar()
    #     return show_toolbar
