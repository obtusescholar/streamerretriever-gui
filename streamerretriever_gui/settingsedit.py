#! /usr/bin/env python3


import os
import sys
import logging

from PySide2.QtWidgets import (
    QDialogButtonBox,
    QStyleFactory,
    QApplication,
    QFileDialog,
    QDialog)
from PySide2.QtCore import QPoint
from PySide2.QtGui import Qt

from . ui_settingsedit import Ui_SettingsEdit
from . twitchload import TwitchLoad
from . import constants


logger = logging.getLogger(__name__)
logger.setLevel(constants.LOG_LEVEL)

file_handler = logging.FileHandler(constants.SETTINGS_LOG)
formatter = logging.Formatter(constants.SETTINGS_FORM)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class SettingsEdit(QDialog):
    def __init__(self, *ignore, parent=None, conf, api):
        super().__init__(parent)
        self.__parent = parent
        self.__api = api
        # self.__conf = self.__api.getConfigs()
        self.__conf = conf

        self.ui = Ui_SettingsEdit()

        self.ui.setupUi(self)
        self.__loadConfigs()
        self.__actions()
        self.setWindowFlag(
            Qt.FramelessWindowHint, self.__conf.showTitlebar)

        self.show()

    def __actions(self):
        self.ui.pushButtonPreview.clicked.connect(self.__preview)
        self.ui.buttonBox.clicked.connect(self.__handleButtons)

        # General
        # self.__iconsSet()
        self.ui.checkBoxEnableIcons.toggled.connect(self.__iconsSet)
        self.ui.checkBoxPlayerSet.toggled.connect(self.__playerSet)
        self.ui.checkBoxChatSet.toggled.connect(self.__chatSet)
        self.ui.toolButtonPlayerChoose.clicked.connect(self.__playerChoose)
        self.ui.toolButtonChatChoose.clicked.connect(self.__chatChoose)
        self.ui.checkBoxAutostarting.toggled.connect(self.__autostartingSet)
        self.ui.pushButtonAutostarting.clicked.connect(
            self.__showAutostartingChooser)
        self.__playerSet()
        self.__autostartingSet()
        self.__chatSet()
        self.ui.checkBoxQssDefault.clicked.connect(self.__qssDefault)
        self.ui.checkBoxQssEnable.clicked.connect(self.__qssToggle)
        self.__qssToggle()
        self.__qssDefault()
        self.ui.toolButtonQssTheme.clicked.connect(self.__qssChoose)

        # Streams Table
        self.ui.pushButtonShowColumns.clicked.connect(
            self.__showColumnsChooser)
        self.ui.pushButtonStretchColumn.clicked.connect(
            self.__showStretchChooser)
        # self.ui.pushButtonFont.clicked.connect(self.__pickFont)

    def __preview(self):
        self.__getValues()

        # self.__loadFont()
        if self.__parent:
            self.__parent.loadConfigs()
            self.__parent.setIcons()
            self.__parent.iconColumnWidth()

    def __getValues(self):
        u = self.ui
        c = self.__conf

        # General
        c.windowSizeX = u.spinBoxX.value()
        c.windowSizeY = u.spinBoxY.value()
        c.interval = u.spinBoxCheckInterval.value()
        c.enableIcons = u.checkBoxEnableIcons.isChecked()
        c.enableTrayIcons = u.checkBoxEnableTrayIcons.isChecked()
        c.showToolbarIcons = u.checkBoxShowToolbarIcons.isChecked()
        c.showTitlebar = u.checkBoxShowTitlebar.isChecked()
        c.showToolbar = u.checkBoxShowToolbar.isChecked()
        c.showSidePanel = u.checkBoxShowSidePanel.isChecked()
        c.notifications = u.checkBoxNotifications.isChecked()
        c.startHidden = u.checkBoxStartHidden.isChecked()
        # c.statusbar = u.checkBoxStatusbar.isChecked()
        c.iconTheme = u.comboBoxIconTheme.currentText()
        c.theme = u.comboBoxTheme.currentText()
        c.qssDefault = u.checkBoxQssDefault.isChecked()
        c.qssEnable = u.checkBoxQssEnable.isChecked()
        c.useMobile = u.checkBoxUseMobile.isChecked()
        c.playerSet = u.checkBoxPlayerSet.isChecked()
        c.player = u.lineEditPlayerCommand.text().split()
        c.autostart = u.checkBoxAutostarting.isChecked()
        c.alwaysChat = u.checkBoxAlwaysChat.isChecked()
        c.chatSet = u.checkBoxChatSet.isChecked()
        c.chat = u.lineEditChatCommand.text().split()

        # Streams Table
        c.rowHeight = u.spinBoxRowHeight.value()
        # c.marginOffset = u.spinBoxMarginOffset.value()
        # c.marginCol = u.spinBoxMarginCol.value()
        c.marginRow = u.spinBoxMarginRow.value()
        c.showGrid = u.checkBoxShowGrid.isChecked()
        c.showCHeaders = u.checkBoxColumnHeader.isChecked()
        c.showRHeaders = u.checkBoxRowHeader.isChecked()
        c.filterAuto = u.checkBoxFilterAuto.isChecked()

    # def __pickFont(self):
    #     current = QFont()
    #     if self.__conf.font:
    #         current.fromString(self.__conf.font)

    #     font = QFontDialog()
    #     (ok, font) = QFontDialog.getFont(current, self)
    #     if ok:
    #         # print(font.toString())
    #         self.__conf.font = font.toString()
    #         # print(font)
    #         # self.__conf.set('font', font.toString())
    #         # self.__conf.save()
    #         # self.__parent.loadConfigurations()

    def __qssChoose(self):
        path = constants.THEME_FOLDER
        path.mkdir(parents=True, exist_ok=True)
        fname = self.__pickFile(folder=str(path))
        if fname[0]:
            self.__conf.qssTheme = fname[0]

    def __pickFile(self, folder=None):
        if folder is None:
            # folders = {
            #     'posix': '/usr/bin',
            #     'nt': 'C:/Program Files'}
            # folder = folders[os.name]
            folder = constants.BIN_FOLDER
        elif folder is False:
            folder = None

        fname = QFileDialog.getOpenFileName(self, 'Open File', folder)
        return fname

    def __playerChoose(self):
        fname = self.__pickFile()
        if fname[0]:
            self.ui.lineEditPlayerCommand.setText(fname[0])

    def __chatChoose(self):
        fname = self.__pickFile()
        if fname[0]:
            self.ui.lineEditChatCommand.setText(fname[0])

    def __toggleSet(self, toggle, rule):
        for value in toggle:
            if rule:
                value.setEnabled(True)
            else:
                value.setEnabled(False)

    def __qssDefault(self):
        toggle = [
                self.ui.checkBoxQssEnable,
                self.ui.toolButtonQssTheme]
        rule = not self.ui.checkBoxQssDefault.isChecked()
        self.__toggleSet(toggle, rule)

    def __qssToggle(self):
        toggle = [self.ui.toolButtonQssTheme]
        rule = self.ui.checkBoxQssEnable.isChecked()
        self.__toggleSet(toggle, rule)

    def __iconsSet(self):
        toggle = [self.ui.checkBoxEnableTrayIcons]
        rule = self.ui.checkBoxEnableIcons.isChecked()
        self.__toggleSet(toggle, rule)

        if rule:
            self.ui.checkBoxEnableTrayIcons.setChecked(True)
            if 'streamer_icon' not in self.__conf.shownColumns:
                self.__api.initIconsKeys()
                self.__conf.shownColumns.insert(0, 'streamer_icon')
        else:
            self.ui.checkBoxEnableTrayIcons.setChecked(False)
            if 'streamer_icon' in self.__conf.shownColumns:
                self.__api.noIcons()

    def __chatSet(self):
        toggle = [
            self.ui.lineEditChatCommand,
            self.ui.toolButtonChatChoose]
        rule = self.ui.checkBoxChatSet.isChecked()
        self.__toggleSet(toggle, rule)

    def __playerSet(self):
        toggle = [
            self.ui.lineEditPlayerCommand,
            self.ui.toolButtonPlayerChoose]
        rule = self.ui.checkBoxPlayerSet.isChecked()

        self.__toggleSet(toggle, rule)

        # if not rule:
        #     self.ui.pushButtonAutostarting.setEnabled(False)
        #     toggle[2].setChecked(False)

    def __autostartingSet(self):
        toggle = [self.ui.pushButtonAutostarting]
        rule = self.ui.checkBoxAutostarting.isChecked()
        self.__toggleSet(toggle, rule)
        if rule:
            self.ui.pushButtonAutostarting.setEnabled(True)

    def __loadConfigs(self):
        u = self.ui

        # General
        u.spinBoxX.setValue(self.__conf.windowSizeX)
        u.spinBoxY.setValue(self.__conf.windowSizeY)
        u.spinBoxCheckInterval.setValue(self.__conf.interval)
        u.checkBoxEnableIcons.setChecked(self.__conf.enableIcons)
        u.checkBoxEnableTrayIcons.setChecked(self.__conf.enableTrayIcons)
        u.checkBoxShowToolbarIcons.setChecked(self.__conf.showToolbarIcons)
        u.checkBoxShowTitlebar.setChecked(self.__conf.showTitlebar)
        u.checkBoxShowToolbar.setChecked(self.__conf.showToolbar)
        u.checkBoxShowSidePanel.setChecked(self.__conf.showSidePanel)
        u.checkBoxNotifications.setChecked(self.__conf.notifications)
        u.checkBoxStartHidden.setChecked(self.__conf.startHidden)
        # u.checkBoxStatusbar.setChecked(self.__conf.statusbar)

        ####################
        # Theming
        u.comboBoxIconTheme.addItems(constants.ICONS_THEMES)
        u.comboBoxIconTheme.setCurrentText(self.__conf.iconTheme)
        styles = [constants.APP_THEMES]
        styles.extend(QStyleFactory.keys())
        u.comboBoxTheme.addItems(styles)
        u.comboBoxTheme.setCurrentText(self.__conf.theme)
        u.checkBoxQssDefault.setChecked(self.__conf.qssDefault)
        u.checkBoxQssEnable.setChecked(self.__conf.qssEnable)

        u.checkBoxUseMobile.setChecked(self.__conf.useMobile)
        u.checkBoxPlayerSet.setChecked(self.__conf.playerSet)
        u.lineEditPlayerCommand.setText(" ".join(self.__conf.player))
        u.checkBoxAutostarting.setChecked(self.__conf.autostart)
        u.checkBoxAlwaysChat.setChecked(self.__conf.alwaysChat)
        u.checkBoxChatSet.setChecked(self.__conf.chatSet)
        u.lineEditChatCommand.setText(" ".join(self.__conf.chat))

        # Streams Table
        u.spinBoxRowHeight.setValue(self.__conf.rowHeight)
        # u.spinBoxMarginOffset.setValue(self.__conf.marginOffset)
        # u.spinBoxMarginCol.setValue(self.__conf.marginCol)
        u.spinBoxMarginRow.setValue(self.__conf.marginRow)
        u.checkBoxShowGrid.setChecked(self.__conf.showGrid)
        u.checkBoxColumnHeader.setChecked(self.__conf.showCHeaders)
        u.checkBoxRowHeader.setChecked(self.__conf.showRHeaders)
        u.checkBoxFilterAuto.setChecked(self.__conf.filterAuto)

        # self.__loadFont()

    # def __loadFont(self):
    #     font = QFont()
    #     if self.__conf.font:
    #         font.fromString(self.__conf.font)
    #     self.setFont(font)

    def __handleButtons(self, button):
        sb = self.ui.buttonBox.standardButton(button)
        if sb == QDialogButtonBox.Ok:
            self.__getValues()
            self.accept()
        elif sb == QDialogButtonBox.Cancel:
            self.reject()

    def __showStretchChooser(self):
        # items = list(self.__conf.shownColumns)
        # selected = list(self.__conf.stretchColumn)

        # objs = self.__api.getKeyObjs()
        # for item in self.__api.getKeyObjs():
        #     if item in self.__conf.shownColumns:
        #         items.append(item)

        #     if item in self.__conf.stretchColumn:
        #         selected.append(item)
        items = []
        for col in self.__conf.shownColumns:
            for item in self.__api.getKeyObjs():
                if col == item:
                    items.append(item)

        selected = []
        for stretch in self.__conf.stretchColumn:
            for item in self.__api.getKeyObjs():
                if stretch == item:
                    selected.append(item)

        stretch = TwitchLoad(
            self.__api,
            items,
            '',
            selected,
            parent=self.__parent)

        if stretch.exec_():
            self.__conf.stretchColumn = self.__parent.chooserList

        # stretch = TwitchLoad(

        # selected = list(self.__conf.shownColumns)
        # sel = selected.index(self.__conf.stretchColumn) or 0
        # table_select = [sel, 0, sel, 0]

        # data = [list(self.__api.getPretty(selected))]

        # orientation = 'column'
        # drag = False

        # chooser = Chooser(
        #     data, orientation, drag, table_select, self.__conf, parent=self)

        # if chooser.exec_():
        #     pretty = self.getChooserList()
        #     print(pretty)
        #     ugly = self.__api.getUgly(pretty)
        #     self.__conf.stretchColumn = ugly

    def __showColumnsChooser(self):
        items = self.__api.getKeyObjs()

        selected = []
        for i, col in enumerate(self.__conf.shownColumns):
            for item in self.__api.getKeyObjs():
                if col == item:
                    selected.append(item)
                    items.remove(item)
                    items.insert(i, item)

        columns = TwitchLoad(
            self.__api,
            items,
            '',
            selected,
            parent=self.__parent)

        if columns.exec_():
            self.__conf.shownColumns = self.__parent.chooserList

        # selected = list(self.__conf.shownColumns)
        # table_select = [0, 0, 0, len(selected) - 1]

        # keys = list(self.__api.getKeys().keys())

        # for key in keys:
        #     if key not in selected:
        #         selected.append(key)
        # data = [list(self.__api.getPretty(selected))]

        # orientation = 'row'
        # drag = True

        # chooser = Chooser(
        #     data, orientation, drag, table_select, self.__conf, parent=self)
        # if chooser.exec_():
        #     pretty = self.getChooserList()
        #     ugly = self.__api.getUgly(pretty)
        #     self.__conf.shownColumns = ugly

    # @property
    # def chooserList(self):
    #     return self.__chooser_list

    # @chooserList.setter
    # def chooserList(self, value):
    #     self.__chooser_list = value

    def __showAutostartingChooser(self):
        # dbs = self.__api.getDb()
        # autostreams = list(self.__conf.autoStreams)
        # selected = [
        #     db.getDispName() for db in dbs if db.getName() in autostreams]
        # if selected:
        #     table_select = [0, 0, len(selected) - 1, 0]
        # else:
        #     table_select = False

        # for db in self.__api.getDb():
        #     if db.getDispName() not in selected:
        #         selected.append(db.getDispName())

        # data = [selected]

        # orientation = 'column'
        # drag = False

        # follows = list(self.__api.getFollows())

        follows = self.__api.getDb()
        auto_objs = []
        for f in follows:
            if f.getName() in self.__conf.autoStreams:
                auto_objs.append(f)

        twitch_load = TwitchLoad(
            self.__api,
            follows,
            '',
            auto_objs,
            parent=self.__parent)
        if twitch_load.exec_():
            self.__conf.autoStreams = self.__parent.chooserList

            # auto_streams = []
            # for db in self.__api.getDb():
            #     if db.getDispName() in self.getChooserList():
            #         auto_streams.append(db.getName())
            # self.__conf.autoStreams = auto_streams

        # chooser = Chooser(
        #     data, orientation, drag, table_select, self.__conf, parent=self)
        # if chooser.exec_():
        #     auto_streams = []
        #     for db in self.__api.getDb():
        #         if db.getDispName() in self.getChooserList():
        #             auto_streams.append(db.getName())
        #     self.__conf.autoStreams = auto_streams

    def setChooserList(self, setList):
        self.__list = setList

    def getChooserList(self):
        return self.__list

    def mousePressEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            self.oldPos = event.globalPos()
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            delta = QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()
        else:
            super().mouseMoveEvent(event)


def main():
    from . import tools

    app = QApplication([])
    twitch_api = tools.StreamGui(tools.TwitchConfigs())
    widget = SettingsEdit(api=twitch_api)
    widget.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
