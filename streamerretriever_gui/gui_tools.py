#! /usr/bin/env python3


from PySide2.QtWidgets import (
    QStyledItemDelegate,
    QAbstractItemView,
    QTableView,
    QStyle)
from PySide2.QtCore import (
    QAbstractTableModel,
    QTextStream,
    QThreadPool,
    QRunnable,
    QObject,
    Signal,
    QFile,
    QSize,
    QRect,
    Slot,
    Qt)
from PySide2.QtGui import QIcon


def loadSheet(app, theme):
    # theme_file = QFile(u':/' + theme)
    theme_file = QFile(theme)
    theme_file.open(QFile.ReadOnly | QFile.Text)
    stream = QTextStream(theme_file)
    app.setStyleSheet(stream.readAll())
    # with open(theme, "r") as f:
    #     _style = f.read()
    #     app.setStyleSheet(_style)


def loadStyle(app, api):
    rules = [
        api.getDefault(),
        api.getQssEnable(),
        api.getQssTheme()]

    if rules[0] or rules[1:]:
        theme = False
        if rules[0]:
            theme = api.getQssDefault()
        elif rules[1] and rules[2]:
            theme = api.getQssTheme()

        if theme:
            loadSheet(app, theme)
    app.setStyle(api.getTheme())


class TableViewMain(QTableView):
    def __init__(self, parent):
        super().__init__(parent)

        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.horizontalHeader().setSectionsMovable(True)
        self.horizontalHeader().setDragEnabled(True)
        self.horizontalHeader().setDragDropMode(QAbstractItemView.InternalMove)

        # self.resizeColumnsToContents()
        self.setHorizontalScrollMode(QAbstractItemView.ScrollPerPixel)

        self.setSortingEnabled(True)
        self.sortByColumn(0, Qt.AscendingOrder)


class WorkerSignals(QObject):
    finished = Signal()
    result = Signal(object)


class WorkerPool():
    def getPool(self):
        threadpool = QThreadPool()
        return threadpool


class Worker(QRunnable):

    def __init__(self, fn):
        super().__init__()
        self.__fn = fn
        self.signals = WorkerSignals()

    @Slot()
    def run(self):
        result = self.__fn()
        self.signals.result.emit(result)
        self.signals.finished.emit()


class TableModel(QAbstractTableModel):

    def __init__(
            self,
            data,
            labels,
            func,
            icons,
            icon_size,
            icon_key,
            *ignore,
            table):
        super().__init__()
        self.__delegateStarted = False
        self.__data = data
        self.__orig = data
        self.__labels = labels
        self.__func = func
        self.__icons = icons
        self.__icon_size = icon_size
        self.__icon_key = icon_key
        self.__table = table

        self.iconDelegate()

    def headerData(self, section, orientation, role):
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            # print(self.__labels[section].getDispName())
            return self.__labels[section].getDispName()
        else:
            return super().headerData(section, orientation, role)

    def sortColumns(self, indexes):
        self.layoutAboutToBeChanged.emit()

        labels = []
        # print(len(indexes))
        # print(len(self.__labels))
        for i in indexes:
            labels.append(self.__labels[i])
            # for r in self.__data:
            #     for c in self.__data[r]

        self.__labels = labels

        self.layoutChanged.emit()

    def columnsChanged(self, labels):
        self.layoutAboutToBeChanged.emit()
        self.__labels = labels

        # if not self.__delegateStarted:
        #     self.iconDelegate()
        # if self.__delegateStarted:
        #     self.delegate.changeData(self.__data)
        self.layoutChanged.emit()

    def newData(self, data):
        self.layoutAboutToBeChanged.emit()
        self.__data = data
        self.__orig = data
        # self.iconDelegate()
        if self.__delegateStarted:
            self.delegate.changeData(self.__data)
        self.layoutChanged.emit()

    def data(self, index, role):
        if role == Qt.DisplayRole:
            row = self.__data[index.row()]
            column = self.__labels[index.column()]
            return self.__func(row, column)
        # elif role == Qt.TextAlignmentRole:
        #     return (Qt.AlignHCenter | Qt.AlignVCenter)

    def getNames(self):
        for data in self.__data:
            yield data[1]

    def getRow(self, row):
        if self.__data:
            return self.__data[row]

    def rowCount(self, index):
        return len(self.__data)

    def columnCount(self, index):
        return len(self.__labels)

    def setIcons(self, icons, icon_size, icon_key):
        self.layoutAboutToBeChanged.emit()
        self.__icons = icons
        self.__icon_size = icon_size
        self.__icon_key = icon_key
        self.iconDelegate()
        self.layoutChanged.emit()

    def iconDelegate(self):
        if self.__icons and self.__icon_key in self.__labels:
            self.__delegateStarted = True
            self.delegate = MyDelegate(
                self.__data,
                self.__icons,
                self.__icon_size)
            icon_index = self.__labels.index(self.__icon_key)
            # print(icon_index)
            self.__table.setItemDelegateForColumn(icon_index, self.delegate)
        # else:
        #     self.__table.setItemDelegateForColumn(
        #         self.__icon_index, QStyledItemDelegate())

    def sort(self, column, order=Qt.AscendingOrder):
        if not self.__data:
            return
        self.layoutAboutToBeChanged.emit()
        rows = self.__data
        column = self.__labels[column]

        if self.__func(rows[0], column) == '':
            self.__data = list(self.__orig)
            if order == Qt.AscendingOrder:
                pass
            elif order == Qt.DescendingOrder:
                self.__data.reverse()

            # self.iconDelegate()
            self.delegate.changeData(self.__data)
            self.layoutChanged.emit()
            return

        try:
            sort_list = [float(self.__func(row, column)) for row in rows]
        except ValueError:
            try:
                sort_list = [self.__func(row, column).lower() for row in rows]
            except AttributeError:
                sort_list = [self.__func(row, column) for row in rows]

        if order == Qt.AscendingOrder:
            rev = False
        elif order == Qt.DescendingOrder:
            rev = True

        no_dups = set(sort_list)
        if len(sort_list) == len(no_dups):
            zipped = zip(sort_list, self.__data)
            new = [x for _, x in sorted(zipped, reverse=rev)]
        else:
            new = self.__sortWithDups(column, rev, sort_list, no_dups)

        self.__data = new

        # self.iconDelegate()
        self.delegate.changeData(self.__data)
        self.layoutChanged.emit()

    def __sortWithDups(self, label, rev, sort_list, no_dups):
        new = []
        sort = sorted(no_dups, reverse=rev)
        for s in sort:
            for data in self.__orig:
                d = ''
                try:
                    d = float(self.__func(data, label))
                except ValueError:
                    try:
                        d = self.__func(data, label).lower()
                    except AttributeError:
                        d = self.__func(data, label)
                # if data.get(label.getName()) == s:
                if d == s:
                    new.append(data)
        return new

    def restoreSorting(self):
        self.layoutAboutToBeChanged.emit()
        self.__data = list(self.__orig)

        # self.iconDelegate()
        self.delegate.changeData(self.__data)
        self.layoutChanged.emit()


class MyDelegate(QStyledItemDelegate):
    def __init__(
            self,
            items,
            icons,
            icon_size,
            parent=None):
        super().__init__()
        self.__database = items
        self.__icons = icons
        self.__icon_size = icon_size
        # self.__parent = parent

    def changeData(self, items):
        self.__database = items

    def getIcon(self, index):
        user_id = self.__database[index.row() % len(self.__icons)].getUserId()
        icon = self.__icons.get(user_id)
        return icon

    # def drawFocus(self, painter, option, rect, widget=None):
    #     if (option.state & QStyle.State_HasFocus) == 0 or not rect.isValid():
    #         return
    #     o = QStyleOptionFocusRect()
    #     # no operator= in python, so we have to do this manually
    #     o.state = option.state
    #     o.direction = option.direction
    #     o.rect = option.rect
    #     o.fontMetrics = option.fontMetrics
    #     o.palette = option.palette

    #     o.state |= QStyle.State_KeyboardFocusChange
    #     o.state |= QStyle.State_Item

    #     if (option.state & QStyle.State_Enabled):
    #         cg = QPalette.Normal
    #     else:
    #         QPalette.Disabled
    #     if (option.state & QStyle.State_Selected):
    #         q = QPalette.Highlight
    #     else:
    #         q = QPalette.Window
    #     o.backgroundColor = option.palette.color(cg, q)

    #     style = widget.style() if widget else QApplication.style()
    #     style.drawPrimitive(QStyle.PE_FrameFocusRect, o, painter, widget)

    def paint(self, painter, option, index):
        # else:
        #     super().paint(painter, option, index)
        # if option.state and QStyle.State_MouseOver:
        #     fillRect(option.rect, Qt.red)
        # if option.state and QStyle.State_MouseOver:
        #     print(index)
        #     print(str(index.row()))
        #     print(QStyle.State_MouseOver)
        #     self.__parent.hoverRow()
        #     option.state |= QStyle.State_MouseOver

        if QStyle.State_Item:
            super().paint(painter, option, index)

        icon = QIcon(self.getIcon(index))
        size = QSize(self.__icon_size, self.__icon_size)
        size_h = size.height()
        size_w = size.width()

        h = option.rect.height()
        w = option.rect.width()
        x = option.rect.x() + w/2 - size_w/2
        y = option.rect.y() + h/2 - size_h/2

        rect = QRect(x, y, size_w, size_h)

        icon.paint(painter, rect, Qt.AlignCenter, mode=QIcon.Normal)

        # self.drawFocus(painter, option, option.rect)
