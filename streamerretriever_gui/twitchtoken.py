#! /usr/bin/env python3


import sys
import logging

from PySide2.QtWidgets import (
    QDialogButtonBox,
    QApplication,
    QMessageBox,
    QDialog)

from . import constants
from . gui_tools import loadSheet
from . ui_twitchtoken import Ui_TwitchToken


logger = logging.getLogger(__name__)
logger.setLevel(constants.LOG_LEVEL)

file_handler = logging.FileHandler(constants.TOKEN_LOG)
formatter = logging.Formatter(constants.TOKEN_FORM)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class TwitchToken(QDialog):
    def __init__(self, state, tools, *ignore, parent):
        super().__init__()
        self.__parent = parent
        self.__re = tools.tokenReCompile(state)

        self.ui = Ui_TwitchToken()
        self.ui.setupUi(self)
        self.ui.buttonBox.clicked.connect(self.__handleButtons)
        self.show()

    def __handleButtons(self, button):
        sb = self.ui.buttonBox.standardButton(button)
        if sb == QDialogButtonBox.Ok:
            self.__testToken()
        elif sb == QDialogButtonBox.Cancel:
            self.reject()

    def __testToken(self):
        line = self.ui.lineEditURL.text()
        match = self.__re.match(line)
        if match:
            self.__parent.setToken(line)
            self.accept()
        else:
            msgbox = QMessageBox()
            msgbox.setText('Token was not found from "Paste URL here".')
            loadSheet(msgbox, constants.DEFAULT_THEME)
            msgbox.exec_()
            self.reject()

    def __retry(self):
        if self.__parent:
            pass
        else:
            pass


def main(parent=None):
    app = QApplication([])
    widget = TwitchToken(parent=None)
    widget.show()

    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
