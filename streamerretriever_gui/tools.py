#! /usr/bin/env python3


import os
import re
import sys
import urllib
import logging
import subprocess
import webbrowser
from pathlib import Path

from streamerretriever import tools
from streamerretriever.streamerretriever import (
    StreamerRetriever,
    Stream,
    Vod,
    Key)

from . import constants
from . import configs
from . import twitchtoken
from . gui_tools import (
    loadSheet,
    WorkerPool,
    Worker)
from . twitchload import TwitchLoad
from . import rc_icons  # noqa: F401

logger = logging.getLogger(__name__)
logger.setLevel(constants.LOG_LEVEL)

file_handler = logging.FileHandler(constants.TOOLS_LOG)
formatter = logging.Formatter(constants.TOOLS_FORM)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class StreamGui():
    __newToken = False

    def __init__(self, conf):
        self.__conf = conf
        self.__tools = tools
        self.__state = tools.generateState()
        token = tools.oauth(self.getToken, self.__state)
        # TODO: GUI create token
        self.__twitch = StreamerRetriever(token)
        self.__db = self.__twitch.getDb()

        # self.__twitch = streamchecker
        # self.__db = db

        self.__initValues()
        self.__loadFiles()

        # if source == 'twitch':
        # self.__follows = self.__twitch.getFollows()
        self.__follows = self.__files_fo['follows'].loadFile(singleLayer=True)
        # TODO: different sources
        # elif source == 'youtube':
        #     self.__conf = YoutubeConfigs()

        sources = ['userdata', 'stream', 'game']
        # TODO: Replace self.__keys with self.__key_objs
        self.__keys = self.__twitch.getKeys(sources)
        self.__key_objs = self.__twitch.getKeyObjs(sources)
        self.__key_icon = Key(
            'streamer_icon', {'twitch': 'streamer_icon', 'pretty': 'Icon'})

        if self.__conf.enableIcons:
            self.yesIcons()
        else:
            self.noIcons()

        # if self.__newToken:
        #     self.__showTwitchLoad()

    @property
    def newToken(self):
        return self.__newToken

    @newToken.setter
    def newToken(self):
        self.__newToken = True

    # @property
    # def chooserList(self):
    #     return self.__chooserList

    # @chooserList.setter
    # def chooserList(self, value):
    #     self.__chooserList = value

    def showTwitchLoad(self, parent_main):
        twitch_follows = self.getTwitchFollows()
        twitch_load = TwitchLoad(
            self,
            twitch_follows,
            self.__follows,
            twitch_follows,
            parent=parent_main)
        loadSheet(twitch_load,  self.getQssDefault())

        if twitch_load.exec_():
            logger.debug('TwitchLoad')
            additions = list(parent_main.chooserList)
            additions.extend(self.__follows)
            self.updateFollows(additions)

    def setToken(self, token):
        self.__token = token

    def getToken(self):
        token_ui = twitchtoken.TwitchToken(
            self.__state,
            self.__tools,
            parent=self)
        loadSheet(token_ui, self.getQssDefault())

        if token_ui.exec_():
            self.__newToken = True
            return self.__token
        else:
            sys.exit()

    def __loadFiles(self):
        self.__files_fo = self.__twitch.getFiles()

        # self.__files_fo['conf'] = self.__conf_fo

    def __initValues(self):
        self.__filt = ''
        self.__pid = {}

    def initIconsKeys(self):
        # TODO: Replace key dict for key objs
        if 'streamer_icon' not in self.__keys:
            icon_dict = {
                'streamer_icon': {'twitch': 'streamer_icon', 'pretty': 'Icon'}}
            self.__keys.update(icon_dict)
            self.__key_objs.insert(0, self.__key_icon)

    def yesIcons(self, fn_complete=False):
        self.initIconsKeys()
        self.__download_icons = StreamIcon()
        self.__download_icons.downloadIcons(self.__db, fn_complete)

        # # monkeypatching instances
        # icons = self.__download_icons.getIcons()
        # dbs = []
        # for db in self.__db:
        #     for icon in icons:
        #         if hash(db) == int(icon):
        #             def getIcon():
        #                 return icons[icon]
        #             db.getIcon = getIcon
        #             dbs.append(db)

    def __start(self, item, chat):

        def process(getCommand):
            command = list(getCommand)
            command.append(url)
            logger.info(f'Playing command: {command}')
            if self.__conf.autostart:
                pid = subprocess.Popen(command, close_fds=True)
                self.__pid[item] = pid
                logger.debug(f'Playing pid: {pid}')
            else:
                subprocess.Popen(command, close_fds=True)

        def startPlay():
            rules = [
                self.__conf.playerSet is True,
                self.__conf.player]
            logger.debug(f'Playing rules: {rules}')

            if all(rules):
                process(self.__conf.player)
            else:
                webbrowser.open(url)

        def startChat():
            rules = [
                self.__conf.chatSet is True,
                self.__conf.chat]
            logger.debug(f'Chat rules: {rules}')

            if all(rules):
                process(self.__conf.chat)
            else:
                webbrowser.open(url)

        url = self.__makeUrl(item, chat)
        if chat:
            startChat()
        else:
            startPlay()

    def getConfigs(self):
        return self.__conf

    def getOnline(self):
        # self.__online = self.__twitch.getOnline()

        online = self.__twitch.getOnline()
        self.__online = []
        for db in self.__db:
            for o in online:
                if db.__hash__() == o.__hash__():
                    self.__online.append(o)

        logger.info(f'Online: {tuple(o.getName() for o in self.__online)}')
        return self.__online

    def getVod(self, user):
        vod = self.__twitch.getVod(user.getUserId())
        logger.debug(
            f'Vod: {user.getDispName()} {tuple(v.getVodId() for v in vod)}')
        return vod

    def getPretty(self, keys):
        pretty = (
            self.__keys[x]['pretty'] if x in self.__keys else x for x in keys)
        return pretty

    def getUgly(self, keys):
        ugly = []
        for key in keys:
            for k in self.__keys:
                if self.__keys[k]['pretty'] == key:
                    ugly.append(k)
        return ugly

    def getKeys(self):
        return self.__keys

    def getKeyObjs(self):
        return self.__key_objs

    def getKeysSource(self, sources):
        return self.__twitch.getKeys(sources)

    def getFollows(self):
        return self.__follows

    def getNotFound(self):
        follows = self.getFollows()
        db = self.getDb()

        db_names = [d.getName() for d in db]
        not_found = [f for f in follows if f not in db_names]
        return not_found

    def regexFollows(self, follows):
        # TODO: move regex to GUI "Add" button with error message?
        # r = re.compile(r'^([a-zA-Z0-9_]+)$')
        r = re.compile(
            r'^((https?://)(www\.|m\.)(twitch\.tv/))?([a-zA-Z0-9_]+)$')

        listing = []
        for follow in follows:
            if r.match(follow):
                listing.append(r.match(follow).groups()[-1])
            else:
                logger.warning('Regex failed')
        # listing = tuple(f for f in follows if r.match(f))
        logger.debug(f'Update follows: {listing}')
        return listing

    def updateFollows(self, follows, fn_complete=False):
        listing = self.regexFollows(follows)

        if listing:
            self.__follows = listing
            self.__twitch.updateDb(self.__follows)
            logger.info('Follows updated to object')
        else:
            self.__follows = listing
            self.__twitch.updateDb([])
            logger.info('[UPDATE_FOLLOWS] Regex returned empty list')
            # raise ValueError('Follows list did not pass regex')

        self.__db = self.__twitch.getDb()
        if self.__conf.enableIcons:
            self.__download_icons.downloadIcons(self.__db, fn_complete)

    def getDb(self):
        return self.__twitch.getDb()

    def filtering(self, filt, listing):
        self.__old = self.__filt
        self.__filt = filt.lower()
        logger.debug(f'Filter list: {tuple(lst.getName() for lst in listing)}')
        logger.debug(f'Filter is: {self.__filt}')

        if self.__filt == '':
            return self.__online

        names = []
        # print(listing)
        # keys = listing[0].getKeys()
        # print(keys)
        # TODO: detect system to remove icon keys
        icons = ['streamer_icon']
        keys = list(c for c in self.__conf.shownColumns if c not in icons)
        logger.debug(f'Filter keys: {keys}')

        for item in listing:
            for k in keys:
                if self.__filt in str(item.get(k)).lower():
                    names.append(item)
                    logger.info(f'Filter found: {item.getDispName()}')
                    logger.debug(f'Filter from: {k}: {item.get(k).lower()}')
                    break
        return names

    def filterAutoPlay(self, item):
        if len(self.__filt) > len(self.__old):
            self.__start(item, chat=False)
            if self.__conf.alwaysChat:
                self.__start(item, chat=True)
            return True
        else:
            return False

    # def start(self, item, default=False):
    #     url = self.__makeUrl(item)

    #     if default:
    #         webbrowser.open(url)
    #     elif self.__conf.alwaysChat:
    #         chat_url = self.__makeUrl(item, chat=True)
    #         self.__play(url)
    #         self.__chat(chat_url)
    #     else:
    #         self.__play(url)

    def buttonCombined(self, item):
        self.__start(item, chat=False)
        if not self.__conf.alwaysChat:
            self.__start(item, chat=True)

    def buttonMain(self, item):
        self.__start(item, chat=False)
        if self.__conf.alwaysChat:
            self.__start(item, chat=True)

    def buttonChat(self, item):
        self.__start(item, chat=True)

    def __makeUrl(self, item, chat):
        if not self.__conf.useMobile:
            base_url = constants.URL['normal']
        else:
            base_url = constants.URL['mobile']

        if isinstance(item, Stream):
            if chat is False:
                url = base_url + '/' + item.getName()
            else:
                url = base_url + '/popout/' + item.getName() + '/chat'
        elif isinstance(item, Vod):
            if chat is False:
                url = base_url + '/videos/' + item.getVodId()
            # else:
            #     url = base_url + '/popout' + item.getName() + '/chat'
        else:
            raise ValueError('Player did not recognize given object')
        return url

    def getTwitchFollows(self):
        return self.__twitch.getTwitchFollows()

    def getInterval(self):
        interval = self.__conf.interval
        if not interval or interval < 10:
            interval = self.__conf.interval
        return interval

    def getIcons(self):
        return self.__download_icons.getIcons(self.__db)

    def noIcons(self):
        # print('no icons')
        shownColumns = self.__conf.shownColumns
        for i, c in enumerate(shownColumns):
            if c == 'streamer_icon':
                del shownColumns[i]
                self.__conf.shownColumns = shownColumns
                # self.__key_objs.remove(self.__key_icon)

        if self.__conf.stretchColumn == 'streamer_icon':
            self.__conf.stretchColumn = ""

        self.__keys = self.__twitch.getKeys()

    def getWindowIcons(self):
        icons = []
        folder = constants.ICONS_FOLDER

        qrc = u':/' + folder
        for k in range(11):
            if k == 0:
                icons.append(qrc + '/streamerretriever.svg')
            else:
                icons.append(qrc + '/streamerretriever' + str(k) + '.svg')
        return icons

    def getDefault(self):
        return self.__conf.qssDefault

    def getTheme(self):
        theme = self.__conf.theme
        return theme

    def getQssEnable(self):
        return self.__conf.qssEnable

    def getQssTheme(self):
        if self.__conf.qssTheme:
            theme = Path(self.__conf.qssTheme)
            if theme.is_file():
                return self.__conf.qssTheme
            else:
                logger.warning('[Custom] Theme not found')

    def getQssDefault(self):
        theme = u':/' + constants.DEFAULT_THEME
        return theme
        # theme = Path(constants.DEFAULT_THEME)
        # if theme.is_file():
        #     return str(theme)
        # else:
        #     logger.warning('[Default] Theme not found')


# TODO: make sure you use new tools
class StreamIcon():
    __folder = constants.ICONS_FOLDER
    __ext = constants.ICONS_EXT

    def __init__(self):
        # self.__streamers = streamchecker.Streamers()
        # self.__db = db
        self.__icons_dict = {}

        # b = self.__streamers.baseFolder()
        b = constants.BASES[os.name]
        self.__base = (
            Path.resolve(Path.expanduser(Path(b) / Path(self.__folder))))
        if not self.__base.exists():
            os.makedirs(self.__base)

        pool = WorkerPool()
        self.__threadpool = pool.getPool()

    def __iconsDict(self, db):
        # for stream in self.__streamers.getDb():
        r = self.__iconsRegex()

        for stream in db:
            match = r.match(stream.getIconUrl())
            ext = match.groups()[-1]
            p = Path(self.__base) / Path(stream.getName() + ext)
            path = Path.resolve(Path.expanduser(p))
            self.__icons_dict[stream.getUserId()] = str(path)

    def downloadIcons(self, db, fn_complete=False):
        # print(fn_complete)
        self.__db = db
        worker = Worker(self.__icons)
        if fn_complete is not False:
            worker.signals.result.connect(fn_complete)
        self.__threadpool.start(worker)

        # self.__streamers.updateFollowsDB()
        # for stream in self.__streamers.getDb():
        # self.__db = db

    def __iconsRegex(self):
        r = re.compile(
            r'(^https://static-cdn.jtvnw.net/jtv_user_pictures/)'
            r'([a-z0-9_-]+)'
            r'(\.png|\.jpeg|\.jpg$)')
        return r

    def __icons(self):
        for stream in self.__db:
            # check url with regex
            r = self.__iconsRegex()
            match = r.match(stream.getIconUrl())

            if match:
                ext = match.groups()[-1]
                path = Path(self.__base) / Path(stream.getName() + ext)
                url = match.group()
                if not path.exists():
                    # checking url with regex
                    urllib.request.urlretrieve(url, str(path))  # nosec

    def getIcons(self, db):
        self.__iconsDict(db)
        return self.__icons_dict


def getSourceAPI():
    general_configs = configs.Configs()
    sources = general_configs.sources

    apis = []
    if 'twitch' in sources:
        # token = tools.oauth(tokenInput)
        # streamchecker = StreamerRetriever(token)
        # db = streamchecker.getDb()
        twitch_api = StreamGui(configs.TwitchConfigs())
        apis.append(twitch_api)

    # TODO: different sources
    # if 'youtube' in sources:
    #     youtube_api = StreamGui(YoutubeConfigs())
    #     apis.append(youtube_api)

    # TODO: api for 'husk' window
    # if no sources:
    #     ghost_api = StreamGui(Conf())
    #     apis.append(ghost_api)

    return apis


def tokenInput():
    url = input()
    return url


if __name__ == '__main__':
    stream_gui = getSourceAPI()

    def testFilter():
        db = stream_gui.getDb()
        stream_gui.filtering('esa', db)

    def testOnline():
        online = stream_gui.getOnline()
        print(online)

    def testPlay(item):
        stream_gui.play(item)

    def testVod():
        db = stream_gui.getDb()
        vod = stream_gui.getVod(db[0])
        return vod

    def testUpdateFollows():
        follows = ['esamarathon', '%$"#_fail', 'gamesdonequick', 'esl_csgo']
        stream_gui.updateFollows(follows)

    def testConfigs():
        configs = stream_gui[0].getConfigs()
        print(configs.getValue('shown_columns'))

    # testFilter()
    # testPlay()
    # testOnline()
    # testVod()
    # testUpdateFollows()
    testConfigs()

    # testPlay(testVod()[0])
