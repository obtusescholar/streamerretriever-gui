# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_changelog.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Changelog(object):
    def setupUi(self, Changelog):
        if not Changelog.objectName():
            Changelog.setObjectName(u"Changelog")
        Changelog.resize(640, 640)
        self.verticalLayout = QVBoxLayout(Changelog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.textBrowserChangelog = QTextBrowser(Changelog)
        self.textBrowserChangelog.setObjectName(u"textBrowserChangelog")

        self.verticalLayout.addWidget(self.textBrowserChangelog)

        self.buttonBox = QDialogButtonBox(Changelog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(Changelog)
        self.buttonBox.accepted.connect(Changelog.accept)
        self.buttonBox.rejected.connect(Changelog.reject)

        QMetaObject.connectSlotsByName(Changelog)
    # setupUi

    def retranslateUi(self, Changelog):
        Changelog.setWindowTitle(QCoreApplication.translate("Changelog", u"Dialog", None))
    # retranslateUi

