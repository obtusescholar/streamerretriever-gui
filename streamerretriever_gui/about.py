#! /usr/bin/env python3


import os
import sys

from PySide2.QtWidgets import (
    QDialogButtonBox,
    QApplication,
    QDialog)
from PySide2.QtCore import (
    QPoint,
    Qt)
from PySide2.QtGui import (
    QPixmap,
    QIcon)

from . ui_about import Ui_About
from . import constants


class About(QDialog):
    def __init__(self, api, parent=None):
        super().__init__(parent)
        self.__api = api
        self.__conf = self.__api.getConfigs()

        window_icon = self.__api.getWindowIcons()[0]
        self.setWindowIcon(QIcon(QPixmap(window_icon)))
        self.setWindowFlag(
            Qt.FramelessWindowHint, self.__conf.showTitlebar)

        self.ui = Ui_About()
        self.ui.setupUi(self)
        self.ui.labelVersion.setText(constants.__version__)
        self.ui.labelAuthor.setText(constants.__author__)

        self.ui.buttonBox.clicked.connect(self.__handleButtons)

        picture = QPixmap(window_icon)
        icon = picture.scaledToHeight(60, mode=Qt.SmoothTransformation)
        self.ui.labelLogo.setPixmap(icon)

        self.show()

    def __handleButtons(self, button):
        sb = self.ui.buttonBox.standardButton(button)
        if sb == QDialogButtonBox.Ok:
            self.accept()

    def mousePressEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            self.oldPos = event.globalPos()
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            delta = QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()
        else:
            super().mouseMoveEvent(event)


def main():
    app = QApplication([])
    widget = About()
    widget.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
