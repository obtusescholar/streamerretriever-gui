# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_maingui.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from . gui_tools import TableViewMain

from . import ui_maingui

class Ui_MainGUI(object):
    def setupUi(self, MainGUI):
        if not MainGUI.objectName():
            MainGUI.setObjectName(u"MainGUI")
        MainGUI.resize(400, 550)
        self.actionFollows = QAction(MainGUI)
        self.actionFollows.setObjectName(u"actionFollows")
        self.actionSettings = QAction(MainGUI)
        self.actionSettings.setObjectName(u"actionSettings")
        self.actionInfo = QAction(MainGUI)
        self.actionInfo.setObjectName(u"actionInfo")
        self.actionUpdate = QAction(MainGUI)
        self.actionUpdate.setObjectName(u"actionUpdate")
        self.actionAbout = QAction(MainGUI)
        self.actionAbout.setObjectName(u"actionAbout")
        self.actionFilter = QAction(MainGUI)
        self.actionFilter.setObjectName(u"actionFilter")
        self.actionClose = QAction(MainGUI)
        self.actionClose.setObjectName(u"actionClose")
        self.actionHide = QAction(MainGUI)
        self.actionHide.setObjectName(u"actionHide")
        self.actionToggleMenubar = QAction(MainGUI)
        self.actionToggleMenubar.setObjectName(u"actionToggleMenubar")
        self.actionSaveOrder = QAction(MainGUI)
        self.actionSaveOrder.setObjectName(u"actionSaveOrder")
        self.actionChangelog = QAction(MainGUI)
        self.actionChangelog.setObjectName(u"actionChangelog")
        self.centralwidget = QWidget(MainGUI)
        self.centralwidget.setObjectName(u"centralwidget")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayoutMain = QHBoxLayout()
        self.horizontalLayoutMain.setObjectName(u"horizontalLayoutMain")
        self.verticalLayoutSidePanel = QVBoxLayout()
        self.verticalLayoutSidePanel.setObjectName(u"verticalLayoutSidePanel")
        self.pushButtonUpdate = QPushButton(self.centralwidget)
        self.pushButtonUpdate.setObjectName(u"pushButtonUpdate")
        self.pushButtonUpdate.setMinimumSize(QSize(40, 40))
        self.pushButtonUpdate.setIconSize(QSize(25, 25))
        self.pushButtonUpdate.setFlat(True)

        self.verticalLayoutSidePanel.addWidget(self.pushButtonUpdate)

        self.pushButtonSettings = QPushButton(self.centralwidget)
        self.pushButtonSettings.setObjectName(u"pushButtonSettings")
        self.pushButtonSettings.setMinimumSize(QSize(40, 40))
        self.pushButtonSettings.setIconSize(QSize(25, 25))
        self.pushButtonSettings.setFlat(True)

        self.verticalLayoutSidePanel.addWidget(self.pushButtonSettings)

        self.pushButtonFollows = QPushButton(self.centralwidget)
        self.pushButtonFollows.setObjectName(u"pushButtonFollows")
        self.pushButtonFollows.setMinimumSize(QSize(40, 40))
        self.pushButtonFollows.setIconSize(QSize(25, 25))
        self.pushButtonFollows.setFlat(True)

        self.verticalLayoutSidePanel.addWidget(self.pushButtonFollows)

        self.verticalSpacer = QSpacerItem(0, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayoutSidePanel.addItem(self.verticalSpacer)


        self.horizontalLayoutMain.addLayout(self.verticalLayoutSidePanel)

        self.verticalLayoutMain = QVBoxLayout()
        self.verticalLayoutMain.setObjectName(u"verticalLayoutMain")
        self.tableView = TableViewMain(self.centralwidget)
        self.tableView.setObjectName(u"tableView")

        self.verticalLayoutMain.addWidget(self.tableView)

        self.horizontalLayoutFilter = QHBoxLayout()
        self.horizontalLayoutFilter.setObjectName(u"horizontalLayoutFilter")
        self.lineEditFilter = QLineEdit(self.centralwidget)
        self.lineEditFilter.setObjectName(u"lineEditFilter")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(1)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.lineEditFilter.sizePolicy().hasHeightForWidth())
        self.lineEditFilter.setSizePolicy(sizePolicy1)
        self.lineEditFilter.setMaximumSize(QSize(16777215, 16777215))

        self.horizontalLayoutFilter.addWidget(self.lineEditFilter)

        self.pushButtonFilterClose = QPushButton(self.centralwidget)
        self.pushButtonFilterClose.setObjectName(u"pushButtonFilterClose")
        self.pushButtonFilterClose.setFlat(True)

        self.horizontalLayoutFilter.addWidget(self.pushButtonFilterClose)


        self.verticalLayoutMain.addLayout(self.horizontalLayoutFilter)


        self.horizontalLayoutMain.addLayout(self.verticalLayoutMain)


        self.verticalLayout.addLayout(self.horizontalLayoutMain)

        self.horizontalLayoutButtons = QHBoxLayout()
        self.horizontalLayoutButtons.setObjectName(u"horizontalLayoutButtons")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayoutButtons.addItem(self.horizontalSpacer)

        self.pushButtonPlayChat = QPushButton(self.centralwidget)
        self.pushButtonPlayChat.setObjectName(u"pushButtonPlayChat")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.pushButtonPlayChat.sizePolicy().hasHeightForWidth())
        self.pushButtonPlayChat.setSizePolicy(sizePolicy2)
        self.pushButtonPlayChat.setMinimumSize(QSize(35, 35))
        self.pushButtonPlayChat.setMaximumSize(QSize(16777215, 16777215))
        self.pushButtonPlayChat.setIconSize(QSize(25, 25))
        self.pushButtonPlayChat.setFlat(True)

        self.horizontalLayoutButtons.addWidget(self.pushButtonPlayChat)

        self.pushButtonPlay = QPushButton(self.centralwidget)
        self.pushButtonPlay.setObjectName(u"pushButtonPlay")
        sizePolicy1.setHeightForWidth(self.pushButtonPlay.sizePolicy().hasHeightForWidth())
        self.pushButtonPlay.setSizePolicy(sizePolicy1)
        self.pushButtonPlay.setMinimumSize(QSize(40, 40))
        self.pushButtonPlay.setMaximumSize(QSize(100, 16777215))
        self.pushButtonPlay.setIconSize(QSize(35, 35))
        self.pushButtonPlay.setFlat(True)

        self.horizontalLayoutButtons.addWidget(self.pushButtonPlay)

        self.pushButtonChat = QPushButton(self.centralwidget)
        self.pushButtonChat.setObjectName(u"pushButtonChat")
        sizePolicy2.setHeightForWidth(self.pushButtonChat.sizePolicy().hasHeightForWidth())
        self.pushButtonChat.setSizePolicy(sizePolicy2)
        self.pushButtonChat.setMinimumSize(QSize(35, 35))
        self.pushButtonChat.setMaximumSize(QSize(16777215, 16777215))
        self.pushButtonChat.setIconSize(QSize(25, 25))
        self.pushButtonChat.setFlat(True)

        self.horizontalLayoutButtons.addWidget(self.pushButtonChat)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayoutButtons.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addLayout(self.horizontalLayoutButtons)

        MainGUI.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainGUI)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 400, 24))
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        sizePolicy3.setHorizontalStretch(1)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.menubar.sizePolicy().hasHeightForWidth())
        self.menubar.setSizePolicy(sizePolicy3)
        self.menuEdit = QMenu(self.menubar)
        self.menuEdit.setObjectName(u"menuEdit")
        sizePolicy4 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.menuEdit.sizePolicy().hasHeightForWidth())
        self.menuEdit.setSizePolicy(sizePolicy4)
        self.menuHelp = QMenu(self.menubar)
        self.menuHelp.setObjectName(u"menuHelp")
        sizePolicy4.setHeightForWidth(self.menuHelp.sizePolicy().hasHeightForWidth())
        self.menuHelp.setSizePolicy(sizePolicy4)
        self.menuFile = QMenu(self.menubar)
        self.menuFile.setObjectName(u"menuFile")
        sizePolicy4.setHeightForWidth(self.menuFile.sizePolicy().hasHeightForWidth())
        self.menuFile.setSizePolicy(sizePolicy4)
        MainGUI.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainGUI)
        self.statusbar.setObjectName(u"statusbar")
        MainGUI.setStatusBar(self.statusbar)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuEdit.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        self.menuEdit.addAction(self.actionSettings)
        self.menuEdit.addAction(self.actionFollows)
        self.menuHelp.addAction(self.actionAbout)
        self.menuHelp.addAction(self.actionChangelog)
        self.menuFile.addAction(self.actionSaveOrder)
        self.menuFile.addAction(self.actionUpdate)
        self.menuFile.addAction(self.actionFilter)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionToggleMenubar)
        self.menuFile.addAction(self.actionHide)
        self.menuFile.addAction(self.actionClose)

        self.retranslateUi(MainGUI)

        QMetaObject.connectSlotsByName(MainGUI)
    # setupUi

    def retranslateUi(self, MainGUI):
        MainGUI.setWindowTitle(QCoreApplication.translate("MainGUI", u"Streamer Retriever", None))
        self.actionFollows.setText(QCoreApplication.translate("MainGUI", u"&Follows", None))
#if QT_CONFIG(tooltip)
        self.actionFollows.setToolTip(QCoreApplication.translate("MainGUI", u"Follows", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionFollows.setShortcut(QCoreApplication.translate("MainGUI", u"Ctrl+D", None))
#endif // QT_CONFIG(shortcut)
        self.actionSettings.setText(QCoreApplication.translate("MainGUI", u"&Settings", None))
#if QT_CONFIG(shortcut)
        self.actionSettings.setShortcut(QCoreApplication.translate("MainGUI", u"Ctrl+E", None))
#endif // QT_CONFIG(shortcut)
        self.actionInfo.setText(QCoreApplication.translate("MainGUI", u"In&fo", None))
        self.actionUpdate.setText(QCoreApplication.translate("MainGUI", u"&Update", None))
#if QT_CONFIG(shortcut)
        self.actionUpdate.setShortcut(QCoreApplication.translate("MainGUI", u"Ctrl+R", None))
#endif // QT_CONFIG(shortcut)
        self.actionAbout.setText(QCoreApplication.translate("MainGUI", u"&About", None))
        self.actionFilter.setText(QCoreApplication.translate("MainGUI", u"&Filter", None))
#if QT_CONFIG(shortcut)
        self.actionFilter.setShortcut(QCoreApplication.translate("MainGUI", u"Ctrl+F", None))
#endif // QT_CONFIG(shortcut)
        self.actionClose.setText(QCoreApplication.translate("MainGUI", u"&Quit", None))
#if QT_CONFIG(shortcut)
        self.actionClose.setShortcut(QCoreApplication.translate("MainGUI", u"Ctrl+Q", None))
#endif // QT_CONFIG(shortcut)
        self.actionHide.setText(QCoreApplication.translate("MainGUI", u"&Hide", None))
#if QT_CONFIG(shortcut)
        self.actionHide.setShortcut(QCoreApplication.translate("MainGUI", u"Ctrl+H", None))
#endif // QT_CONFIG(shortcut)
        self.actionToggleMenubar.setText(QCoreApplication.translate("MainGUI", u"Toggle Menubar", None))
#if QT_CONFIG(shortcut)
        self.actionToggleMenubar.setShortcut(QCoreApplication.translate("MainGUI", u"Ctrl+Shift+M", None))
#endif // QT_CONFIG(shortcut)
        self.actionSaveOrder.setText(QCoreApplication.translate("MainGUI", u"&Save columns", None))
#if QT_CONFIG(shortcut)
        self.actionSaveOrder.setShortcut(QCoreApplication.translate("MainGUI", u"Ctrl+S", None))
#endif // QT_CONFIG(shortcut)
        self.actionChangelog.setText(QCoreApplication.translate("MainGUI", u"&Changelog", None))
#if QT_CONFIG(tooltip)
        self.pushButtonUpdate.setToolTip(QCoreApplication.translate("MainGUI", u"<html><head/><body><p>Update</p><p>&lt;Ctrl+R&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonUpdate.setText("")
#if QT_CONFIG(tooltip)
        self.pushButtonSettings.setToolTip(QCoreApplication.translate("MainGUI", u"<html><head/><body><p>Settings</p><p>&lt;Ctrl+E&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonSettings.setText("")
#if QT_CONFIG(tooltip)
        self.pushButtonFollows.setToolTip(QCoreApplication.translate("MainGUI", u"<html><head/><body><p>Database</p><p>&lt;Ctrl+D&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonFollows.setText("")
#if QT_CONFIG(tooltip)
        self.lineEditFilter.setToolTip(QCoreApplication.translate("MainGUI", u"Type to filter online streams.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.lineEditFilter.setWhatsThis(QCoreApplication.translate("MainGUI", u"Type to filter online streams. Press Enter to start playing selected stream.", None))
#endif // QT_CONFIG(whatsthis)
        self.lineEditFilter.setPlaceholderText(QCoreApplication.translate("MainGUI", u"Filter", None))
#if QT_CONFIG(tooltip)
        self.pushButtonFilterClose.setToolTip(QCoreApplication.translate("MainGUI", u"<html><head/><body><p>Close filter</p><p>&lt;Esc&gt;</p><p>&lt;Ctrl+F&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonFilterClose.setText("")
#if QT_CONFIG(tooltip)
        self.pushButtonPlayChat.setToolTip(QCoreApplication.translate("MainGUI", u"<html><head/><body><p>Start playing selected stream and open chat.</p><p>&lt;Ctrl+Enter&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonPlayChat.setText("")
#if QT_CONFIG(shortcut)
        self.pushButtonPlayChat.setShortcut(QCoreApplication.translate("MainGUI", u"Ctrl+Return", None))
#endif // QT_CONFIG(shortcut)
#if QT_CONFIG(tooltip)
        self.pushButtonPlay.setToolTip(QCoreApplication.translate("MainGUI", u"<html><head/><body><p>Start playing selected stream.</p><p>&lt;Enter&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.pushButtonPlay.setWhatsThis(QCoreApplication.translate("MainGUI", u"Start playing selected stream.", None))
#endif // QT_CONFIG(whatsthis)
        self.pushButtonPlay.setText("")
#if QT_CONFIG(tooltip)
        self.pushButtonChat.setToolTip(QCoreApplication.translate("MainGUI", u"<html><head/><body><p>Open selected streams chat.</p><p>&lt;Alt+Enter&gt;</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
        self.pushButtonChat.setText("")
#if QT_CONFIG(shortcut)
        self.pushButtonChat.setShortcut(QCoreApplication.translate("MainGUI", u"Alt+Return", None))
#endif // QT_CONFIG(shortcut)
        self.menuEdit.setTitle(QCoreApplication.translate("MainGUI", u"&Edit", None))
        self.menuHelp.setTitle(QCoreApplication.translate("MainGUI", u"&Help", None))
        self.menuFile.setTitle(QCoreApplication.translate("MainGUI", u"&File", None))
    # retranslateUi

