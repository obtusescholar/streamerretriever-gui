# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_twitchtoken.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_TwitchToken(object):
    def setupUi(self, TwitchToken):
        if not TwitchToken.objectName():
            TwitchToken.setObjectName(u"TwitchToken")
        TwitchToken.resize(750, 395)
        self.verticalLayout = QVBoxLayout(TwitchToken)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.textBrowser = QTextBrowser(TwitchToken)
        self.textBrowser.setObjectName(u"textBrowser")

        self.verticalLayout.addWidget(self.textBrowser)

        self.label = QLabel(TwitchToken)
        self.label.setObjectName(u"label")

        self.verticalLayout.addWidget(self.label)

        self.lineEditURL = QLineEdit(TwitchToken)
        self.lineEditURL.setObjectName(u"lineEditURL")

        self.verticalLayout.addWidget(self.lineEditURL)

        self.buttonBox = QDialogButtonBox(TwitchToken)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(TwitchToken)

        QMetaObject.connectSlotsByName(TwitchToken)
    # setupUi

    def retranslateUi(self, TwitchToken):
        TwitchToken.setWindowTitle(QCoreApplication.translate("TwitchToken", u"Streamer Retriever - OAuth token", None))
        self.textBrowser.setHtml(QCoreApplication.translate("TwitchToken", u"<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Ubuntu'; font-size:10pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans Serif'; font-size:16pt;\">Generating Twitch API OAuth token</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Sans Serif'; font-size:9pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans Serif'; font-size:9pt;\">Current Twitch API requires each user to generate their own Twit"
                        "ch API token. Twitch page has been opened into your default web browser asking your permission to generate the token.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Sans Serif'; font-size:9pt;\"><br /></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans Serif'; font-size:9pt; font-style:italic;\">If you already closed the browser window you can press the </span><span style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:600; font-style:italic;\">Cancel</span><span style=\" font-family:'Sans Serif'; font-size:9pt; font-style:italic;\"> button and reopen Streamchecker.</span><span style=\" font-family:'Sans Serif'; font-size:9pt;\"><br /><br /></span><span style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:600;\">1. Open the page, login into your Twitch account a"
                        "nd give permission for Streamer Retriever.</span><span style=\" font-family:'Sans Serif'; font-size:9pt;\"><br /></span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans Serif'; font-size:9pt; font-style:italic;\">After giving permission for Streamer Retriever you are loaded into a page that looks like there was an error. That is OK.</span><span style=\" font-family:'Sans Serif'; font-size:9pt;\"><br /></span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:'Sans Serif'; font-size:9pt; font-weight:600;\">2. Copy the URL from your web browser into the text input area down below and click OK.<br /></span><span style=\" font-family:'Sans Serif'; font-size:9pt;\"><br /></span><span style=\" font-family:'Sans Serif'; font-size:9pt; font-style:italic;\">The URL looks something like this:<br />https:"
                        "//localhost/#access_token=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&amp;scope=&amp;state=XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX&amp;token_type=bearer</span></p></body></html>", None))
        self.label.setText(QCoreApplication.translate("TwitchToken", u"Paste URL here:", None))
    # retranslateUi

