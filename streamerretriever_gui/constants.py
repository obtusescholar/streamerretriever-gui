#! /usr/bin/env python3


import os
import logging
from pathlib import Path


__version__ = '0.20.6'
__author__ = 'obtusescholar'

BASES = {
    'posix': '~/.config/StreamerRetriever/',
    'nt': './config/'}


def getBase():
    str_path = BASES[os.name]
    abs_path = Path.resolve(Path.expanduser(Path(str_path)))
    return abs_path


BASE = getBase()

URL = {
    'normal': 'https://www.twitch.tv',
    'mobile': 'https://m.twitch.tv'}

FOLLOWS = 'follows.csv'

DB = 'streamerretreiver.json'
CONFIG = 'gui_conf.json'

TWITCH = 'twitch.csv'

ICONS_FOLDER = 'icons'
ICONS_EXT = '.png'
ICONS_BUTTONS = 'icons/buttons/'
ICONS_THEMES = ['stonewall-dark', 'stonewall-light']
APP_THEMES = [None]


def getTheme():
    folder = 'themes/'
    rel_path = Path(BASES[os.name]) / Path(folder)
    abs_path = Path.resolve(Path.expanduser(rel_path))
    return abs_path


THEME_FOLDER = getTheme()


def getBin():
    bin_folders = {
        'posix': '/usr/bin/',
        'nt': 'C:/Program Files/'}
    return bin_folders[os.name]


BIN_FOLDER = getBin()


def getLog(log, key):
    logs = {
        'unified': {
            'file': 'streamerretriever.log'},
        'chooser': {
            'file': 'gui_chooser.log',
            'tag':  '[CHOOSER]'},
        'config': {
            'file': 'gui_config.log',
            'tag':  '[CONFIG]'},
        'follows': {
            'file': 'gui_follows.log',
            'tag':  '[FOLLOWS]'},
        'gui': {
            'file': 'gui_main.log',
            'tag':  '[MAIN]'},
        'gui_tools': {
            'file': 'gui_qt_tools.log',
            'tag':  '[GUI_TOOLS]'},
        'settings': {
            'file': 'gui_settings.log',
            'tag':  '[SETTINGS]'},
        'token': {
            'file': 'gui_token.log',
            'tag':  '[TOKEN]'},
        'tools': {
            'file': 'gui_tools.log',
            'tag': '[GUI_TOOLS]'}}
    return logs[log][key]


def getLogFile(log):
    # folder = 'logs/'

    # log_folder = Path(BASES[os.name]) / Path(folder)
    log_folder = Path(BASES[os.name])
    # rel_path = log_folder / Path(getLog(log, 'file'))
    rel_path = log_folder / Path(getLog('unified', 'file'))
    abs_path = Path.resolve(Path.expanduser(rel_path))

    return abs_path


LOG_LEVEL = logging.WARNING
TOOLS_LOG = getLogFile('tools')
GUI_LOG = getLogFile('gui')
GUI_TOOLS_LOG = getLogFile('gui_tools')
FOLLOWS_LOG = getLogFile('follows')
SETTINGS_LOG = getLogFile('settings')
CONFIG_LOG = getLogFile('config')
CHOOSER_LOG = getLogFile('chooser')
TOKEN_LOG = getLogFile('token')


def getLogForm(log):
    log_form = '[%(asctime)s] [%(levelname)s] %(message)s'
    logger_form = '[GUI]' + ' ' + getLog(log, 'tag') + ' ' + log_form
    return logger_form


TOOLS_FORM = getLogForm('tools')
GUI_FORM = getLogForm('gui')
GUI_TOOLS_FORM = getLogForm('gui_tools')
FOLLOWS_FORM = getLogForm('follows')
SETTINGS_FORM = getLogForm('settings')
CONFIG_FORM = getLogForm('config')
CHOOSER_FORM = getLogForm('chooser')
TOKEN_FORM = getLogForm('token')


DEFAULT_THEME = 'themes/stonewall-dark.qss'
