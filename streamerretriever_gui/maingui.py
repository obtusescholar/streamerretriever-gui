#! /usr/bin/env python3


import os
import sys
import copy
import logging
from functools import partial

from PySide2.QtWidgets import (
    QStyledItemDelegate,
    QSystemTrayIcon,
    QApplication,
    QHeaderView,
    QMainWindow,
    QMenu)
from PySide2.QtCore import (
    QThreadPool,
    QPoint,
    QTimer,
    Qt)
from PySide2.QtGui import (
    QPixmap,
    QIcon)

from . ui_maingui import Ui_MainGUI
from . gui_tools import (
    TableModel,
    loadStyle,
    Worker)
from . import about
from . import changelog
from . import settingsedit
from . import followedit
from . import constants
from . import tools
from . import rc_icons  # noqa: F401


logger = logging.getLogger(__name__)
logger.setLevel(constants.LOG_LEVEL)

file_handler = logging.FileHandler(constants.GUI_LOG)
formatter = logging.Formatter(constants.GUI_FORM)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class MainGui(QMainWindow):

    def __init__(self, api):
        super().__init__()
        self.__api = api
        self.__conf = self.__api.getConfigs()

        self.__online = []
        self.__autostarted = []

        self.__threadpool = QThreadPool()
        self.__intervalLoop()

        self.__icons = self.__api.getWindowIcons()
        self.setWindowIcon(QIcon(QPixmap(self.__icons[0])))
        self.setWindowFlag(
            Qt.FramelessWindowHint, self.__conf.showTitlebar)

        # init systemtray
        self.tray = QSystemTrayIcon(self)
        if self.tray.isSystemTrayAvailable():
            if os.name == 'nt':
                string = 'Streamer Retriever-GUI'
            else:
                string = (
                    '<html><head/><body>' +
                    '<p><b>Streamer Retriever</b></p>' +
                    '&lt;MB1&gt;Show&verbar;Normalize<br/>' +
                    '&lt;MB2&gt;Menu<br/>' +
                    '&lt;MB3&gt;Show&verbar;Hide' +
                    '</p></body></html>')
            self.tray.setToolTip(string)
            self.tray.activated.connect(self.__trayClicked)
            self.__ctmenu = QMenu()
        else:
            self.tray = None

        self.__icon_index = False
        # init ui
        self.ui = Ui_MainGUI()
        self.ui.setupUi(self)
        self.__toolbarActions()
        self.__filterHide()
        self.__actions()

        self.__first_data = True
        if self.__api.newToken:
            self.__api.showTwitchLoad(self)

        self.loadInitConfigs()
        self.__intervalStart()

        if self.__conf.startHidden:
            self.hide()
        else:
            self.show()

        self.__tableSettings()
        self.__tableInit()

    def __hide(self):
        if not self.__conf.showToolbar:
            self.ui.menubar.setMaximumHeight(0)

        self.hide()

    def __buttonIcons(self):
        t = self.__conf.iconTheme

        ####################
        # Toolbar
        if self.__conf.showToolbarIcons:
            tool_file = QIcon(QPixmap(
                u':/' + constants.ICONS_BUTTONS + t + '/file.svg'))
            self.ui.menuFile.setIcon(tool_file)

            tools = QIcon(QPixmap(
                u':/' + constants.ICONS_BUTTONS + t + '/tools.svg'))
            self.ui.menuEdit.setIcon(tools)

            tool_help = QIcon(QPixmap(
                u':/' + constants.ICONS_BUTTONS + t + '/help.svg'))
            self.ui.menuHelp.setIcon(tool_help)
        else:
            self.ui.menuFile.setIcon(QIcon())
            self.ui.menuEdit.setIcon(QIcon())
            self.ui.menuHelp.setIcon(QIcon())

        ####################
        # Side Panel
        update = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/reload.svg'))
        self.ui.pushButtonUpdate.setIcon(update)

        follows = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/pen.svg'))
        self.ui.pushButtonFollows.setIcon(follows)

        settings = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/cog.svg'))
        self.ui.pushButtonSettings.setIcon(settings)

        ####################
        # Filter
        close_filter = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/stop.svg'))
        self.ui.pushButtonFilterClose.setIcon(close_filter)

        ####################
        # Bottom Panel
        if self.__conf.alwaysChat:
            play_chat_icon = '/play.svg'
            play_icon = '/chat_play.svg'
        else:
            play_chat_icon = '/chat_play.svg'
            play_icon = '/play.svg'

        chat_play = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + play_chat_icon))
        self.ui.pushButtonPlayChat.setIcon(chat_play)

        play = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + play_icon))
        self.ui.pushButtonPlay.setIcon(play)

        chat = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/chat.svg'))
        self.ui.pushButtonChat.setIcon(chat)

        ####################
        # Menu
        self.ui.actionUpdate.setIcon(update)
        self.ui.actionFollows.setIcon(follows)
        self.ui.actionSettings.setIcon(settings)

        door = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/door.svg'))
        self.ui.actionClose.setIcon(door)

        search = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/search.svg'))
        self.ui.actionFilter.setIcon(search)

        columns = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/columns.svg'))
        self.ui.actionSaveOrder.setIcon(columns)

        menubar = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/menubar.svg'))
        self.ui.actionToggleMenubar.setIcon(menubar)

        hide = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/hide.svg'))
        self.ui.actionHide.setIcon(hide)

        phone = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/phone.svg'))
        self.ui.actionAbout.setIcon(phone)

        changelog = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/list.svg'))
        self.ui.actionChangelog.setIcon(changelog)

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            self.ui.tableView.clearSelection()
            self.ui.lineEditFilter.setFocus()
            if self.ui.lineEditFilter.maximumHeight() != 0:
                self.__filterClose()
            else:
                self.ui.lineEditFilter.clearFocus()
        elif e.key() == Qt.Key_Alt:
            if not self.__conf.showToolbar:
                self.ui.menubar.setMaximumHeight(16777215)
        elif e.key() == Qt.Key_Return:
            self.ui.pushButtonPlay.click()
            self.__filterClose()

    def keyReleaseEvent(self, e):
        if e.key() == Qt.Key_Alt and not self.__conf.showToolbar:
            self.ui.menubar.setMaximumHeight(0)

    def __actions(self):
        self.ui.lineEditFilter.textChanged.connect(self.__filter)
        self.ui.pushButtonFilterClose.clicked.connect(self.__filterClose)
        # self.ui.lineEditFilter.returnPressed.connect(self.__filterPlay)
        self.ui.tableView.doubleClicked.connect(self.__buttonMain)

        # Play Buttons
        self.ui.pushButtonPlay.clicked.connect(self.__buttonMain)
        self.ui.pushButtonChat.clicked.connect(self.__buttonChat)
        self.ui.pushButtonPlayChat.clicked.connect(self.__buttonPlay)

        # Side Buttons
        self.ui.pushButtonUpdate.clicked.connect(self.__intervalStart)
        self.ui.pushButtonFollows.clicked.connect(self.__showFollowEdit)
        self.ui.pushButtonSettings.clicked.connect(self.__showSettingsEdit)

    def __toolbarActions(self):
        self.__toolbarShow()

        # File
        self.ui.actionSaveOrder.triggered.connect(self.__saveOrder)
        self.ui.actionUpdate.triggered.connect(self.__intervalStart)
        self.ui.actionFilter.triggered.connect(self.__filterToggle)
        self.ui.actionToggleMenubar.triggered.connect(self.__menubarToggle)
        self.ui.actionHide.triggered.connect(self.hide)
        self.ui.actionClose.triggered.connect(self.close)

        # Edit
        self.ui.actionFollows.triggered.connect(self.__showFollowEdit)
        self.ui.actionSettings.triggered.connect(self.__showSettingsEdit)

        # Help
        self.ui.actionAbout.triggered.connect(self.__showAbout)
        self.ui.actionChangelog.triggered.connect(self.__showChangelog)

    def __saveOrder(self):
        header = self.ui.tableView.horizontalHeader()
        header.headerDataChanged(
            Qt.Horizontal, 0, len(self.__conf.shownColumns))

        # labels = []
        # columns = self.__conf.shownColumns
        # for col in range(len(columns)):
        #     i = header.logicalIndex(col)
        #     labels.append(columns[i])

        labels = []
        logical_indexes = []
        for col in range(self.model.columnCount(0)):
            i = header.logicalIndex(col)
            labels.append(self.__conf.shownColumns[i])
            logical_indexes.append(i)

        # if 'streamer_icon' in labels:
        #     labels.remove('streamer_icon')
        #     labels.insert(self.__icon_index, 'streamer_icon')

        # header = self.ui.tableView.horizontalHeader()

        # header.headerDataChanged(Qt.Horizontal, 0, len(columns))

        self.__conf.shownColumns = labels
        self.__conf.save()

        # self.loadConfigs()
        # print(self.__conf.shownColumns)
        # self.__conf.shownColumns = labels

        # self.__conf.save()
        # self.__tableLabels()

        # self.model.updateLabels(logical_indexes)

        # self.loadConfigs()

        # header.restoreState(self.__header_state)
        # self.model.sortColumns(logical_indexes)

    def __showToggle(self):
        if self.isVisible():
            self.__hide()
        else:
            self.show()

    def __trayNotify(self, recent):
        names = ' '.join(r.getDispName() for r in recent)
        if self.tray and self.__conf.notifications:
            self.tray.showMessage(
                'Recently Started\n', names, QSystemTrayIcon.Information, 4000)

    def __trayClicked(self, reason):
        if reason == QSystemTrayIcon.MiddleClick:
            self.__showToggle()
        elif reason == QSystemTrayIcon.DoubleClick:
            self.__showSettingsEdit()
        elif reason == QSystemTrayIcon.Trigger:
            if self.isHidden():
                self.show()
            elif self.isMaximized():
                self.showNormal()
                self.activateWindow()
            elif self.isMinimized():
                self.showNormal()
                self.activateWindow()
            elif self.isActiveWindow():
                self.showMinimized()
            elif not self.isActiveWindow():
                self.activateWindow()

    def __trayPopulate(self, items):
        self.__ctmenu.clear()

        if self.__conf.enableIcons:
            icons = self.__api.getIcons()

        if items:
            action_items = []
            self.tray.setIcon(QIcon(QPixmap(self.__icons[len(items)])))
            for i, item in enumerate(items):
                if self.__conf.enableIcons and self.__conf.enableTrayIcons:
                    user_icon = QPixmap(icons[item.getUserId()])
                    action_items.append(
                        self.__ctmenu.addAction(user_icon, item.getDispName()))
                else:
                    action_items.append(
                        self.__ctmenu.addAction(item.getDispName()))
                action_items[i].triggered.connect(
                    partial(self.__api.buttonMain, item))
        else:
            self.tray.setIcon(QIcon(QPixmap(self.__icons[0])))

        self.__ctmenu.addSeparator()
        action_show = self.__ctmenu.addAction('Show | Hide')
        action_show.triggered.connect(self.__showToggle)
        action_update = self.__ctmenu.addAction('Update')
        action_update.triggered.connect(self.__intervalStart)
        action_quit = self.__ctmenu.addAction('Quit')
        action_quit.triggered.connect(self.close)

        self.tray.setContextMenu(self.__ctmenu)
        self.tray.show()

    def __menubarToggle(self):
        if self.ui.menubar.maximumHeight() == 0:
            self.ui.menubar.setMaximumHeight(16777215)
        else:
            self.ui.menubar.setMaximumHeight(0)

    def __toolbarShow(self):
        if self.__conf.showToolbar:
            self.ui.menubar.setMaximumHeight(16777215)
        else:
            self.ui.menubar.setMaximumHeight(0)

    def __sidePanelToggle(self):
        items = [
            self.ui.pushButtonUpdate,
            self.ui.pushButtonFollows,
            self.ui.pushButtonSettings]

        if self.__conf.showSidePanel:
            for item in items:
                item.setMaximumWidth(16777215)
                item.setMinimumWidth(16777215)
                self.ui.horizontalLayoutMain.setSpacing(6)
        else:
            for item in items:
                item.setMaximumWidth(0)
                item.setMinimumWidth(0)
                self.ui.horizontalLayoutMain.setSpacing(0)

    def __getSelected(self):
        table = self.ui.tableView
        selected = table.selectionModel().selectedRows()

        if selected:
            row = selected[0].row()
        else:
            row = 0
            table.selectRow(row)

        # selected_item = self.__table_items[row]
        selected_item = self.model.getRow(row)
        if selected_item:
            logger.info(f'Filter Playing: {selected_item.getDispName()}')
            return selected_item
        else:
            logger.info('No items found in table')

    def __buttonMain(self):
        selected_item = self.__getSelected()
        if selected_item:
            self.__api.buttonMain(selected_item)
            self.ui.tableView.clearSelection()

    def __buttonPlay(self):
        selected_item = self.__getSelected()
        if selected_item:
            self.__api.buttonCombined(selected_item)
            self.ui.tableView.clearSelection()

    def __buttonChat(self):
        selected_item = self.__getSelected()
        if selected_item:
            self.__api.buttonChat(selected_item)
            self.ui.tableView.clearSelection()

    def __filter(self):
        txt = self.ui.lineEditFilter.text().lower()
        self.__filtered = self.__api.filtering(txt, self.__online)
        self.tableData(self.__filtered)
        self.ui.tableView.selectRow(0)

        if self.__conf.filterAuto:
            self.__filterStartAuto()

    def __filterStartAuto(self):
        if self.model.rowCount(0) == 1:
            if self.__api.filterAutoPlay(self.__filtered[0]):
                self.__filterClose()

    def __filterPlay(self):
        self.__filter()
        self.__buttonMain()
        self.__filterClose()

    def __filterHide(self):
        self.ui.lineEditFilter.clearFocus()
        self.ui.lineEditFilter.setMaximumHeight(0)
        self.ui.pushButtonFilterClose.setMaximumHeight(0)
        self.ui.horizontalLayoutFilter.setSpacing(0)
        self.ui.verticalLayoutMain.setSpacing(0)

    def __filterClose(self):
        self.ui.lineEditFilter.setText('')
        self.ui.tableView.clearSelection()
        self.__filterHide()

    def __filterShow(self):
        self.ui.lineEditFilter.setFocus()
        self.ui.lineEditFilter.setMaximumHeight(16777215)
        self.ui.pushButtonFilterClose.setMaximumHeight(16777215)
        self.ui.horizontalLayoutFilter.setSpacing(6)
        self.ui.verticalLayoutMain.setSpacing(6)

    def __filterToggle(self):
        if self.ui.lineEditFilter.maximumHeight() == 0:
            self.__filterShow()
        else:
            self.__filterClose()

    def __newStreams(self, online):
        if self.__online:
            old = set(o for o in self.__online)
            new = set(n for n in online)
            recent = new - old
        else:
            recent = set(o for o in online)

        if recent and recent != set():
            self.__trayNotify(recent)
            if self.__conf.autostart:
                self.__autoStart(recent)

    def __autoStart(self, recent):
        for r in recent:
            rules = [
                r.getName() in self.__conf.autoStreams,
                r.getStreamId() not in self.__autostarted]

            if all(rules):
                self.__autostarted.append(r.getStreamId())
                self.__api.buttonMain(r)

    def __intervalStore(self, online):
        self.__newStreams(online)
        self.__online = online

        # self.__tablePopulate(self.__online)
        # self.model.newData(self.__online)

        if self.__first_data:
            self.__first_data = False
            self.__tableInit(self.__online)
        else:
            self.tableData(self.__online)

        if self.tray is not None:
            self.__trayPopulate(self.__online)

    def __intervalStart(self):
        worker = Worker(self.__intervalDo)
        worker.signals.result.connect(self.__intervalStore)
        self.__threadpool.start(worker)

    def __intervalDo(self):
        online = self.__api.getOnline()
        return online

    def __intervalLoop(self):
        self.__timer = QTimer()
        self.__timer.setTimerType(Qt.VeryCoarseTimer)
        self.__timer.timeout.connect(self.__intervalStart)
        self.__timer.start(self.__conf.interval*60*1000)

    def __tableLabels(self):
        cols = self.__conf.shownColumns
        objs = self.__api.getKeyObjs()

        # cols = []
        # for c in col:
        #     for obj in objs:
        #         if c == obj:
        #             cols.append(obj)

        labels = []
        for col in cols:
            i = objs.index(col)
            labels.append(objs[i])

        return labels

    def __tableInit(self, items=False):
        if not items:
            items = []

        cols = self.__tableLabels()
        icon_key = 'streamer_icon'
        rules = [
            self.__conf.enableIcons,
            icon_key in self.__conf.shownColumns]

        if all(rules):
            icons = self.__api.getIcons()
            icon_size = self.__conf.rowHeight - self.__conf.marginRow * 2
        else:
            icons = None
            icon_size = None

        self.model = TableModel(
            items,
            cols,
            self.__objsToData,
            icons,
            icon_size,
            icon_key,
            table=self.ui.tableView)
        self.ui.tableView.setModel(self.model)

        # self.ui.tableView.resizeColumnsToContents()
        self.__tableSettings()
        self.__tableStretch()

    def tableData(self, items=False):
        # if self.__conf.enableIcons:
        #     icon_key = 'streamer_icon'
        self.model.newData(items)
        # self.__tableStretch()

    def __tableStretch(self):
        # if self.__online:
        # for stretch in self.__conf.stretchColumn:
        #     if stretch in self.__conf.shownColumns:
        #         index = self.__conf.shownColumns.index(stretch)
        #         header = self.ui.tableView.horizontalHeader()
        #         header.setSectionResizeMode(index, QHeaderView.Stretch)
        header = self.ui.tableView.horizontalHeader()
        for i, c in enumerate(self.__conf.shownColumns):
            if c in self.__conf.stretchColumn:
                header.setSectionResizeMode(i, QHeaderView.Stretch)
            else:
                header.setSectionResizeMode(
                    i, QHeaderView.ResizeToContents)
        self.iconColumnWidth()

    def iconColumnWidth(self):
        width = self.ui.tableView.columnWidth(self.__icon_index)
        icon_size = self.__conf.rowHeight - self.__conf.marginRow * 2
        if width < icon_size:
            header = self.ui.tableView.horizontalHeader()
            header.setSectionResizeMode(self.__icon_index, QHeaderView.Fixed)
            header.resizeSection(self.__icon_index, icon_size)

    def __tableSettings(self):
        vertical = self.ui.tableView.verticalHeader()
        vertical.setVisible(self.__conf.showRHeaders)
        vertical.setDefaultSectionSize(self.__conf.rowHeight)

        header = self.ui.tableView.horizontalHeader()
        header.setVisible(self.__conf.showCHeaders)

        self.ui.tableView.setWordWrap(False)

        if self.__conf.showGrid and not self.ui.tableView.showGrid():
            self.ui.tableView.setShowGrid(True)
        elif not self.__conf.showGrid and self.ui.tableView.showGrid():
            self.ui.tableView.setShowGrid(False)

        # self.__header_state = header.saveState()

    @staticmethod
    def __objsToData(obj, key):
        methods = {
            'display_name': obj.getDispName(),
            'game': obj.getGame(),
            'title': obj.getTitle(),
            'viewers': obj.getViewers(),
            'time': obj.getTime(),
            'user_id': obj.getUserId(),
            'name': obj.getName(),
            'type': obj.getType(),
            'partner': obj.getPartner(),
            'description': obj.getDesc(),
            'icon_url': obj.getIconUrl(),
            'offline_image': obj.getOfflineImage(),
            'view_count': obj.getViewCount(),
            'stream_id': obj.getStreamId(),
            'game_id': obj.getGameId(),
            'started_at': obj.getStarted(),
            'thumbnail_url': obj.getThumbUrl(),
            'tag_ids': obj.getTagIds(),
            'box_art_url': obj.getBoxArtUrl()}

        if key.getName() != 'streamer_icon':
            return methods[key.getName()]
        else:
            return ''

    def redrawTable(self):
        self.tableData(self.__online)

    def __showFollowEdit(self):
        if not self.__conf.showToolbar:
            self.ui.menubar.setMaximumHeight(0)

        follow_edit = followedit.FollowEdit(parent=self, api=self.__api)
        if follow_edit.exec_():
            self.__intervalStart()
        follow_edit.deleteLater()

    def __showAbout(self):
        about_ = about.About(api=self.__api, parent=self)
        about_.show()

    def __showChangelog(self):
        changelog_ = changelog.Changelog(api=self.__api, parent=self)
        changelog_.show()

    def __showSettingsEdit(self):
        if not self.__conf.showToolbar:
            self.ui.menubar.setMaximumHeight(0)

        old_conf = copy.deepcopy(self.__conf)
        settings_edit = settingsedit.SettingsEdit(
            parent=self, conf=self.__conf, api=self.__api)

        if settings_edit.exec_():
            self.__conf.save()
        else:
            self.__conf = copy.deepcopy(old_conf)

        self.loadConfigs()

        labels = self.__tableLabels()

        self.model.columnsChanged(labels)
        self.setIcons()
        self.__tableStretch()

    def setIcons(self):
        if self.__conf.enableIcons:
            icon_key = 'streamer_icon'
            if icon_key in self.__conf.shownColumns:
                self.__api.yesIcons(self.redrawTable)
                icons = self.__api.getIcons()
                icon_size = self.__conf.rowHeight - self.__conf.marginRow * 2
                self.model.setIcons(icons, icon_size, icon_key)
            else:
                self.__api.yesIcons()
        else:
            self.__api.noIcons()
            if self.__icon_index is not False:
                self.ui.tableView.setItemDelegateForColumn(
                    self.__icon_index, QStyledItemDelegate())

        # self.ui.tableView.resizeColumnsToContents()

    def __loadWindowSize(self):
        x = self.__conf.windowSizeX
        y = self.__conf.windowSizeY
        self.resize(x, y)

    # def __loadFont(self):
    #     font = QFont()
    #     if self.__conf.font:
    #         font.fromString(self.__conf.font)
    #     self.setFont(font)

    #     ####################
    #     # Menubar
    #     self.ui.menubar.setFont(font)

    #     ####################
    #     # Tray
    #     self.__ctmenu.setFont(font)

    #     ####################
    #     # Filter
    #     self.ui.lineEditFilter.setFont(font)

    #     ####################
    #     # Table
    #     table = self.ui.tableView
    #     table.setFont(font)
    #     table.horizontalHeader().setFont(font)

    def loadInitConfigs(self):
        if self.__timer.interval() / 1000 / 60 != self.__conf.interval:
            self.__timer.stop()
            self.__intervalLoop()

        if self.__conf.enableIcons:
            self.__api.yesIcons()
        else:
            self.__api.noIcons()

        if self.__conf.statusbar and self.ui.statusbar.isHidden():
            self.ui.statusbar.show()
        elif not self.__conf.statusbar and not self.ui.statusbar.isHidden():
            self.ui.statusbar.hide()

        self.__loadWindowSize()
        # self.__loadFont()
        self.__sidePanelToggle()
        self.__toolbarShow()
        self.__buttonIcons()

        icon_key = 'streamer_icon'
        icon_rules = [
            self.__conf.enableIcons,
            icon_key in self.__conf.shownColumns]
        if all(icon_rules):
            self.__icon_index = self.__conf.shownColumns.index(icon_key)

    def loadConfigs(self):
        self.loadInitConfigs()

        # self.__tablePopulate(self.__online)
        self.__tableSettings()
        self.__trayPopulate(self.__online)

    def mousePressEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            self.oldPos = event.globalPos()
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            delta = QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()
        else:
            super().mouseMoveEvent(event)

    def closeEvent(self, event):
        self.__timer.stop()
        QApplication.quit()
        event.accept()

    @property
    def chooserList(self):
        return self.__chooser_list

    @chooserList.setter
    def chooserList(self, value):
        self.__chooser_list = value


def main():
    app = QApplication([])
    app.setApplicationName('Streamer Retriever')
    app.setOrganizationName(constants.__author__)
    app.setAttribute(Qt.AA_UseHighDpiPixmaps)

    apis = tools.getSourceAPI()

    for api in apis:
        window = MainGui(api)  # noqa: F841
        # window = MainGui(api)
        # window.show()

    # rules = [
    #     api.getDefault(),
    #     api.getQssEnable(),
    #     api.getQssTheme()]

    # if rules[0] or rules[1:]:
    #     theme = False
    #     if rules[0]:
    #         theme = QFile(u':/' + api.getQssDefault())
    #         # theme = QFile(u':/themes/stonewall-dark.qss')
    #         # theme = u':/themes/stonewall-dark.qss'
    #     elif rules[1] and rules[2]:
    #         theme = api.getQssTheme()

    #     if theme:
    #         theme.open(QFile.ReadOnly | QFile.Text)
    #         stream = QTextStream(theme)
    #         app.setStyleSheet(stream.readAll())
    #         # with open(theme, "r") as f:
    #         #     _style = f.read()
    #         #     app.setStyleSheet(_style)
    #         # logger.info('[App] Theme applied')
    app.setStyle(api.getTheme())

    loadStyle(app, api)

    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
