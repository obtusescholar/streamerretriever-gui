#! /usr/bin/env python3


import os
import sys

from PySide2.QtCore import (
    QSize,
    QMetaObject,
    QCoreApplication,
    QPoint,
    QRect)
from PySide2.QtGui import (
    QIcon,
    Qt,
    QPixmap)
from PySide2.QtWidgets import (
    QPushButton,
    QSizePolicy,
    QDialog,
    QCheckBox,
    QVBoxLayout,
    QHBoxLayout,
    QSpacerItem,
    QScrollArea,
    QWidget,
    QDialogButtonBox,
    QApplication)

from . import constants


class Ui_Dialog():

    def buttonSettings(self, button):
        button.setSizePolicy(self.buttonSize(self.pushButton1))
        button.setMinimumSize(QSize(40, 40))
        self.verticalLayout.addWidget(button)

    def buttonSize(self, button):
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(button.sizePolicy().hasHeightForWidth())
        return sizePolicy

    def buttonCreate(self, dialog):
        self.pushButton1 = QPushButton(dialog)
        self.pushButton1 .setObjectName(u"pushButton1")

        self.pushButton2 = QPushButton(dialog)
        self.pushButton2 .setObjectName(u"pushButton2")

        self.buttonSettings(self.pushButton1)
        self.buttonSettings(self.pushButton2)

    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(480, 680)

        self.verticalLayout_2 = QVBoxLayout(Dialog)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")

        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.setObjectName(u"verticalLayout")

        self.buttonCreate(Dialog)

        self.verticalSpacer = QSpacerItem(
            20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.horizontalLayout.addLayout(self.verticalLayout)
        self.horizontalLayout.setStretch(1, 1)

        self.scrollArea = QScrollArea()
        self.scrollArea.setObjectName(u"scrollArea")
        self.scrollArea.setWidgetResizable(True)

        self.scrollAreaWidgetContents = QWidget()
        self.scrollAreaWidgetContents.setObjectName(
            u"scrollAreaWidgetContents")
        self.scrollAreaWidgetContents.setGeometry(QRect(0, 0, 464, 590))

        self.verticalLayoutCheckBoxes = QVBoxLayout(
            self.scrollAreaWidgetContents)
        self.verticalLayoutCheckBoxes.setObjectName(
            u"verticalLayoutCheckBoxes")

        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.horizontalLayout.addWidget(self.scrollArea)

        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.buttonBox = QDialogButtonBox(Dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(
            QDialogButtonBox.Cancel | QDialogButtonBox.Ok)

        self.verticalLayout_2.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(
            QCoreApplication.translate("Dialog", u"Dialog", None))

        # if QT_CONFIG(tooltip)
        tip1 = (
            u"<html><head/><body><p>Select all</p>"
            u"<p>&lt;Alt+A&gt;</p></body></html>")
        self.pushButton1.setToolTip(
            QCoreApplication.translate("Dialog", tip1, None))
        # endif // QT_CONFIG(tooltip)
        self.pushButton1.setText("")

        # if QT_CONFIG(tooltip)
        tip2 = (
            u"<html><head/><body><p>Select none</p>"
            u"<p>&lt;Alt+D&gt;</p></body></html>")
        self.pushButton2.setToolTip(
            QCoreApplication.translate("Dialog", tip2, None))
        # endif // QT_CONFIG(tooltip)
        self.pushButton2.setText("")
        # retranslateUi


class TwitchLoad(QDialog):

    def __init__(
            self,
            api,
            items,
            filitems,
            checked,
            *ignore,
            parent=None):
        super().__init__(parent)
        self.__parent = parent
        self.__api = api
        self.__conf = self.__api.getConfigs()

        self.__items = items
        self.__filitems = filitems
        self.__checked = checked

        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.buttonBox.clicked.connect(self.__handleButtons)

        # self.__follows = list(self.__api.getFollows())
        self.__boxes = self.__createBoxes()
        self.__buttons()

        self.setWindowFlag(
            Qt.FramelessWindowHint, self.__conf.showTitlebar)

    def __handleButtons(self, button):
        sb = self.ui.buttonBox.standardButton(button)
        if sb == QDialogButtonBox.Ok:
            additions = []
            for box, item in self.__boxes:
                if box.isChecked():
                    additions.append(item.getName())

            self.__parent.chooserList = additions
            # additions.extend(self.__follows)
            # self.__api.updateFollows(additions)
            self.accept()
        elif sb == QDialogButtonBox.Cancel:
            self.reject()

    def __selectAll(self):
        for box, _ in self.__boxes:
            box.setChecked(True)

    def __selectNone(self):
        for box, _ in self.__boxes:
            box.setChecked(False)

    def __buttons(self):
        t = self.__conf.iconTheme

        plus = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/plus.svg'))
        self.ui.pushButton1.setIcon(plus)
        self.ui.pushButton1.clicked.connect(self.__selectAll)

        minus = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/minus.svg'))
        self.ui.pushButton2.setIcon(minus)
        self.ui.pushButton2.clicked.connect(self.__selectNone)

    def __createBoxes(self):
        boxes = []
        for item in self.__items:
            if item.getName() in self.__filitems:
                continue
            box = QCheckBox(f'{item.getDispName()}')
            if item in self.__checked:
                box.setChecked(True)
            self.ui.verticalLayoutCheckBoxes.addWidget(box)
            boxes.append((box, item))
        else:
            verticalSpacerBoxes = QSpacerItem(
                20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
            self.ui.verticalLayoutCheckBoxes.addItem(verticalSpacerBoxes)
        return boxes

    def mousePressEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            self.oldPos = event.globalPos()
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            delta = QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()
        else:
            super().mouseMoveEvent(event)


def main():
    app = QApplication([])
    widget = TwitchLoad()
    widget.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
