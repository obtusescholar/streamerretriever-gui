# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_settingsedit.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_SettingsEdit(object):
    def setupUi(self, SettingsEdit):
        if not SettingsEdit.objectName():
            SettingsEdit.setObjectName(u"SettingsEdit")
        SettingsEdit.resize(800, 514)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(SettingsEdit.sizePolicy().hasHeightForWidth())
        SettingsEdit.setSizePolicy(sizePolicy)
        self.gridLayout_11 = QGridLayout(SettingsEdit)
        self.gridLayout_11.setObjectName(u"gridLayout_11")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.pushButtonPreview = QPushButton(SettingsEdit)
        self.pushButtonPreview.setObjectName(u"pushButtonPreview")

        self.horizontalLayout.addWidget(self.pushButtonPreview)

        self.buttonBox = QDialogButtonBox(SettingsEdit)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setMinimumSize(QSize(111, 28))
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.horizontalLayout.addWidget(self.buttonBox)


        self.gridLayout_11.addLayout(self.horizontalLayout, 2, 0, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_11.addItem(self.verticalSpacer, 1, 0, 1, 1)

        self.tabWidget = QTabWidget(SettingsEdit)
        self.tabWidget.setObjectName(u"tabWidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy1)
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayout_10 = QGridLayout(self.tab)
        self.gridLayout_10.setObjectName(u"gridLayout_10")
        self.checkBoxNotifications = QCheckBox(self.tab)
        self.checkBoxNotifications.setObjectName(u"checkBoxNotifications")

        self.gridLayout_10.addWidget(self.checkBoxNotifications, 0, 0, 1, 1)

        self.labelCheckInterval = QLabel(self.tab)
        self.labelCheckInterval.setObjectName(u"labelCheckInterval")
        self.labelCheckInterval.setMinimumSize(QSize(0, 28))

        self.gridLayout_10.addWidget(self.labelCheckInterval, 1, 0, 1, 1)

        self.spinBoxCheckInterval = QSpinBox(self.tab)
        self.spinBoxCheckInterval.setObjectName(u"spinBoxCheckInterval")
        sizePolicy2 = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.spinBoxCheckInterval.sizePolicy().hasHeightForWidth())
        self.spinBoxCheckInterval.setSizePolicy(sizePolicy2)
        self.spinBoxCheckInterval.setMinimumSize(QSize(120, 28))
        self.spinBoxCheckInterval.setMinimum(10)
        self.spinBoxCheckInterval.setValue(30)

        self.gridLayout_10.addWidget(self.spinBoxCheckInterval, 1, 1, 1, 1)

        self.groupBox_2 = QGroupBox(self.tab)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.gridLayout_3 = QGridLayout(self.groupBox_2)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.checkBoxUseMobile = QCheckBox(self.groupBox_2)
        self.checkBoxUseMobile.setObjectName(u"checkBoxUseMobile")

        self.gridLayout_3.addWidget(self.checkBoxUseMobile, 0, 0, 1, 1)

        self.checkBoxPlayerSet = QCheckBox(self.groupBox_2)
        self.checkBoxPlayerSet.setObjectName(u"checkBoxPlayerSet")

        self.gridLayout_3.addWidget(self.checkBoxPlayerSet, 1, 0, 1, 1)

        self.lineEditPlayerCommand = QLineEdit(self.groupBox_2)
        self.lineEditPlayerCommand.setObjectName(u"lineEditPlayerCommand")
        self.lineEditPlayerCommand.setEnabled(True)
        self.lineEditPlayerCommand.setMinimumSize(QSize(0, 28))

        self.gridLayout_3.addWidget(self.lineEditPlayerCommand, 2, 0, 1, 1)

        self.toolButtonPlayerChoose = QToolButton(self.groupBox_2)
        self.toolButtonPlayerChoose.setObjectName(u"toolButtonPlayerChoose")
        self.toolButtonPlayerChoose.setEnabled(True)
        self.toolButtonPlayerChoose.setMinimumSize(QSize(120, 28))

        self.gridLayout_3.addWidget(self.toolButtonPlayerChoose, 2, 1, 1, 1)

        self.checkBoxAutostarting = QCheckBox(self.groupBox_2)
        self.checkBoxAutostarting.setObjectName(u"checkBoxAutostarting")

        self.gridLayout_3.addWidget(self.checkBoxAutostarting, 3, 0, 1, 1)

        self.pushButtonAutostarting = QPushButton(self.groupBox_2)
        self.pushButtonAutostarting.setObjectName(u"pushButtonAutostarting")
        sizePolicy2.setHeightForWidth(self.pushButtonAutostarting.sizePolicy().hasHeightForWidth())
        self.pushButtonAutostarting.setSizePolicy(sizePolicy2)
        self.pushButtonAutostarting.setMinimumSize(QSize(120, 28))

        self.gridLayout_3.addWidget(self.pushButtonAutostarting, 3, 1, 1, 1)


        self.gridLayout_10.addWidget(self.groupBox_2, 2, 0, 1, 2)

        self.groupBox_4 = QGroupBox(self.tab)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.gridLayout_4 = QGridLayout(self.groupBox_4)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.checkBoxAlwaysChat = QCheckBox(self.groupBox_4)
        self.checkBoxAlwaysChat.setObjectName(u"checkBoxAlwaysChat")

        self.gridLayout_4.addWidget(self.checkBoxAlwaysChat, 0, 0, 1, 1)

        self.lineEditChatCommand = QLineEdit(self.groupBox_4)
        self.lineEditChatCommand.setObjectName(u"lineEditChatCommand")
        self.lineEditChatCommand.setMinimumSize(QSize(0, 28))

        self.gridLayout_4.addWidget(self.lineEditChatCommand, 2, 0, 1, 1)

        self.toolButtonChatChoose = QToolButton(self.groupBox_4)
        self.toolButtonChatChoose.setObjectName(u"toolButtonChatChoose")
        self.toolButtonChatChoose.setMinimumSize(QSize(120, 28))

        self.gridLayout_4.addWidget(self.toolButtonChatChoose, 2, 1, 1, 1)

        self.checkBoxChatSet = QCheckBox(self.groupBox_4)
        self.checkBoxChatSet.setObjectName(u"checkBoxChatSet")

        self.gridLayout_4.addWidget(self.checkBoxChatSet, 1, 0, 1, 1)


        self.gridLayout_10.addWidget(self.groupBox_4, 3, 0, 1, 2)

        self.verticalSpacer_4 = QSpacerItem(20, 0, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_10.addItem(self.verticalSpacer_4, 4, 0, 1, 2)

        self.tabWidget.addTab(self.tab, "")
        self.tab_3 = QWidget()
        self.tab_3.setObjectName(u"tab_3")
        self.gridLayout_6 = QGridLayout(self.tab_3)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.labelX = QLabel(self.tab_3)
        self.labelX.setObjectName(u"labelX")
        self.labelX.setMinimumSize(QSize(0, 28))

        self.gridLayout_6.addWidget(self.labelX, 0, 1, 1, 1)

        self.spinBoxX = QSpinBox(self.tab_3)
        self.spinBoxX.setObjectName(u"spinBoxX")
        sizePolicy2.setHeightForWidth(self.spinBoxX.sizePolicy().hasHeightForWidth())
        self.spinBoxX.setSizePolicy(sizePolicy2)
        self.spinBoxX.setMinimumSize(QSize(120, 28))
        self.spinBoxX.setMinimum(10)
        self.spinBoxX.setMaximum(9999)

        self.gridLayout_6.addWidget(self.spinBoxX, 0, 2, 1, 1)

        self.groupBox = QGroupBox(self.tab_3)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout_9 = QGridLayout(self.groupBox)
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.gridLayout_7 = QGridLayout()
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.checkBoxEnableTrayIcons = QCheckBox(self.groupBox)
        self.checkBoxEnableTrayIcons.setObjectName(u"checkBoxEnableTrayIcons")

        self.gridLayout_7.addWidget(self.checkBoxEnableTrayIcons, 0, 1, 1, 1)

        self.checkBoxEnableIcons = QCheckBox(self.groupBox)
        self.checkBoxEnableIcons.setObjectName(u"checkBoxEnableIcons")

        self.gridLayout_7.addWidget(self.checkBoxEnableIcons, 0, 0, 1, 1)

        self.checkBoxShowToolbarIcons = QCheckBox(self.groupBox)
        self.checkBoxShowToolbarIcons.setObjectName(u"checkBoxShowToolbarIcons")

        self.gridLayout_7.addWidget(self.checkBoxShowToolbarIcons, 1, 0, 1, 1)


        self.gridLayout_9.addLayout(self.gridLayout_7, 1, 0, 1, 2)

        self.gridLayout_9.setColumnStretch(0, 1)

        self.gridLayout_6.addWidget(self.groupBox, 4, 0, 1, 3)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.checkBoxShowTitlebar = QCheckBox(self.tab_3)
        self.checkBoxShowTitlebar.setObjectName(u"checkBoxShowTitlebar")
        self.checkBoxShowTitlebar.setTristate(False)

        self.gridLayout_2.addWidget(self.checkBoxShowTitlebar, 0, 0, 1, 1)

        self.checkBoxStartHidden = QCheckBox(self.tab_3)
        self.checkBoxStartHidden.setObjectName(u"checkBoxStartHidden")

        self.gridLayout_2.addWidget(self.checkBoxStartHidden, 0, 1, 1, 1)

        self.checkBoxShowSidePanel = QCheckBox(self.tab_3)
        self.checkBoxShowSidePanel.setObjectName(u"checkBoxShowSidePanel")

        self.gridLayout_2.addWidget(self.checkBoxShowSidePanel, 1, 1, 1, 1)

        self.checkBoxShowToolbar = QCheckBox(self.tab_3)
        self.checkBoxShowToolbar.setObjectName(u"checkBoxShowToolbar")

        self.gridLayout_2.addWidget(self.checkBoxShowToolbar, 1, 0, 1, 1)


        self.gridLayout_6.addLayout(self.gridLayout_2, 2, 0, 1, 3)

        self.spinBoxY = QSpinBox(self.tab_3)
        self.spinBoxY.setObjectName(u"spinBoxY")
        sizePolicy2.setHeightForWidth(self.spinBoxY.sizePolicy().hasHeightForWidth())
        self.spinBoxY.setSizePolicy(sizePolicy2)
        self.spinBoxY.setMinimumSize(QSize(120, 28))
        self.spinBoxY.setMinimum(10)
        self.spinBoxY.setMaximum(9999)

        self.gridLayout_6.addWidget(self.spinBoxY, 1, 2, 1, 1)

        self.groupBox_3 = QGroupBox(self.tab_3)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.gridLayout_13 = QGridLayout(self.groupBox_3)
        self.gridLayout_13.setObjectName(u"gridLayout_13")
        self.label_8 = QLabel(self.groupBox_3)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout_13.addWidget(self.label_8, 2, 0, 1, 1)

        self.label_3 = QLabel(self.groupBox_3)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_13.addWidget(self.label_3, 0, 0, 1, 1)

        self.comboBoxTheme = QComboBox(self.groupBox_3)
        self.comboBoxTheme.setObjectName(u"comboBoxTheme")
        sizePolicy2.setHeightForWidth(self.comboBoxTheme.sizePolicy().hasHeightForWidth())
        self.comboBoxTheme.setSizePolicy(sizePolicy2)
        self.comboBoxTheme.setMinimumSize(QSize(120, 28))

        self.gridLayout_13.addWidget(self.comboBoxTheme, 2, 1, 1, 1)

        self.checkBoxQssEnable = QCheckBox(self.groupBox_3)
        self.checkBoxQssEnable.setObjectName(u"checkBoxQssEnable")

        self.gridLayout_13.addWidget(self.checkBoxQssEnable, 4, 0, 1, 1)

        self.toolButtonQssTheme = QToolButton(self.groupBox_3)
        self.toolButtonQssTheme.setObjectName(u"toolButtonQssTheme")
        self.toolButtonQssTheme.setMinimumSize(QSize(120, 28))

        self.gridLayout_13.addWidget(self.toolButtonQssTheme, 4, 1, 1, 1)

        self.comboBoxIconTheme = QComboBox(self.groupBox_3)
        self.comboBoxIconTheme.setObjectName(u"comboBoxIconTheme")
        sizePolicy2.setHeightForWidth(self.comboBoxIconTheme.sizePolicy().hasHeightForWidth())
        self.comboBoxIconTheme.setSizePolicy(sizePolicy2)
        self.comboBoxIconTheme.setMinimumSize(QSize(120, 28))

        self.gridLayout_13.addWidget(self.comboBoxIconTheme, 0, 1, 1, 1)

        self.checkBoxQssDefault = QCheckBox(self.groupBox_3)
        self.checkBoxQssDefault.setObjectName(u"checkBoxQssDefault")

        self.gridLayout_13.addWidget(self.checkBoxQssDefault, 3, 0, 1, 1)


        self.gridLayout_6.addWidget(self.groupBox_3, 3, 0, 1, 3)

        self.label_6 = QLabel(self.tab_3)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setMinimumSize(QSize(0, 28))

        self.gridLayout_6.addWidget(self.label_6, 0, 0, 1, 1)

        self.labelY = QLabel(self.tab_3)
        self.labelY.setObjectName(u"labelY")
        self.labelY.setMinimumSize(QSize(0, 28))

        self.gridLayout_6.addWidget(self.labelY, 1, 1, 1, 1)

        self.gridLayout_6.setColumnStretch(1, 1)
        self.tabWidget.addTab(self.tab_3, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.gridLayout_12 = QGridLayout(self.tab_2)
        self.gridLayout_12.setObjectName(u"gridLayout_12")
        self.groupBoxFilter = QGroupBox(self.tab_2)
        self.groupBoxFilter.setObjectName(u"groupBoxFilter")
        self.gridLayout_8 = QGridLayout(self.groupBoxFilter)
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.horizontalSpacer_5 = QSpacerItem(84, 28, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_8.addItem(self.horizontalSpacer_5, 0, 1, 1, 1)

        self.label_4 = QLabel(self.groupBoxFilter)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setMinimumSize(QSize(0, 28))

        self.gridLayout_8.addWidget(self.label_4, 1, 0, 1, 1)

        self.label_10 = QLabel(self.groupBoxFilter)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout_8.addWidget(self.label_10, 1, 1, 1, 1)

        self.label = QLabel(self.groupBoxFilter)
        self.label.setObjectName(u"label")

        self.gridLayout_8.addWidget(self.label, 3, 0, 1, 2)

        self.label_2 = QLabel(self.groupBoxFilter)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout_8.addWidget(self.label_2, 4, 0, 1, 2)

        self.label_5 = QLabel(self.groupBoxFilter)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setMinimumSize(QSize(0, 28))

        self.gridLayout_8.addWidget(self.label_5, 0, 0, 1, 1)

        self.spinBoxRowHeight = QSpinBox(self.groupBoxFilter)
        self.spinBoxRowHeight.setObjectName(u"spinBoxRowHeight")
        sizePolicy2.setHeightForWidth(self.spinBoxRowHeight.sizePolicy().hasHeightForWidth())
        self.spinBoxRowHeight.setSizePolicy(sizePolicy2)
        self.spinBoxRowHeight.setMinimumSize(QSize(120, 28))
        self.spinBoxRowHeight.setMinimum(20)

        self.gridLayout_8.addWidget(self.spinBoxRowHeight, 0, 2, 1, 1)

        self.spinBoxMarginRow = QSpinBox(self.groupBoxFilter)
        self.spinBoxMarginRow.setObjectName(u"spinBoxMarginRow")
        sizePolicy2.setHeightForWidth(self.spinBoxMarginRow.sizePolicy().hasHeightForWidth())
        self.spinBoxMarginRow.setSizePolicy(sizePolicy2)
        self.spinBoxMarginRow.setMinimumSize(QSize(120, 28))

        self.gridLayout_8.addWidget(self.spinBoxMarginRow, 1, 2, 1, 1)

        self.pushButtonShowColumns = QPushButton(self.groupBoxFilter)
        self.pushButtonShowColumns.setObjectName(u"pushButtonShowColumns")
        sizePolicy2.setHeightForWidth(self.pushButtonShowColumns.sizePolicy().hasHeightForWidth())
        self.pushButtonShowColumns.setSizePolicy(sizePolicy2)
        self.pushButtonShowColumns.setMinimumSize(QSize(120, 28))

        self.gridLayout_8.addWidget(self.pushButtonShowColumns, 3, 2, 1, 1)

        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.checkBoxColumnHeader = QCheckBox(self.groupBoxFilter)
        self.checkBoxColumnHeader.setObjectName(u"checkBoxColumnHeader")

        self.gridLayout.addWidget(self.checkBoxColumnHeader, 0, 0, 1, 1)

        self.checkBoxRowHeader = QCheckBox(self.groupBoxFilter)
        self.checkBoxRowHeader.setObjectName(u"checkBoxRowHeader")

        self.gridLayout.addWidget(self.checkBoxRowHeader, 0, 1, 1, 1)

        self.checkBoxShowGrid = QCheckBox(self.groupBoxFilter)
        self.checkBoxShowGrid.setObjectName(u"checkBoxShowGrid")

        self.gridLayout.addWidget(self.checkBoxShowGrid, 1, 0, 1, 1)


        self.gridLayout_8.addLayout(self.gridLayout, 2, 0, 1, 3)

        self.pushButtonStretchColumn = QPushButton(self.groupBoxFilter)
        self.pushButtonStretchColumn.setObjectName(u"pushButtonStretchColumn")
        sizePolicy2.setHeightForWidth(self.pushButtonStretchColumn.sizePolicy().hasHeightForWidth())
        self.pushButtonStretchColumn.setSizePolicy(sizePolicy2)
        self.pushButtonStretchColumn.setMinimumSize(QSize(120, 28))

        self.gridLayout_8.addWidget(self.pushButtonStretchColumn, 4, 2, 1, 1)


        self.gridLayout_12.addWidget(self.groupBoxFilter, 0, 0, 1, 1)

        self.groupBoxUi = QGroupBox(self.tab_2)
        self.groupBoxUi.setObjectName(u"groupBoxUi")
        self.gridLayout_5 = QGridLayout(self.groupBoxUi)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.checkBoxFilterAuto = QCheckBox(self.groupBoxUi)
        self.checkBoxFilterAuto.setObjectName(u"checkBoxFilterAuto")

        self.gridLayout_5.addWidget(self.checkBoxFilterAuto, 0, 0, 1, 1)


        self.gridLayout_12.addWidget(self.groupBoxUi, 1, 0, 1, 1)

        self.verticalSpacer_2 = QSpacerItem(20, 48, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_12.addItem(self.verticalSpacer_2, 2, 0, 1, 1)

        self.tabWidget.addTab(self.tab_2, "")

        self.gridLayout_11.addWidget(self.tabWidget, 0, 0, 1, 1)


        self.retranslateUi(SettingsEdit)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(SettingsEdit)
    # setupUi

    def retranslateUi(self, SettingsEdit):
        SettingsEdit.setWindowTitle(QCoreApplication.translate("SettingsEdit", u"Streamer Retriever - Settings", None))
#if QT_CONFIG(tooltip)
        self.pushButtonPreview.setToolTip(QCoreApplication.translate("SettingsEdit", u"Preview setting changes to the Main Window.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.pushButtonPreview.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Shows you how the Main Window wuold look like with current settings.</p><p>Press Cancel to if you don't like the changes.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.pushButtonPreview.setText(QCoreApplication.translate("SettingsEdit", u"Preview", None))
#if QT_CONFIG(tooltip)
        self.checkBoxNotifications.setToolTip(QCoreApplication.translate("SettingsEdit", u"Show notification for recent shows.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxNotifications.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Enable showing notifications of the recently started shows.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxNotifications.setText(QCoreApplication.translate("SettingsEdit", u"Enable notifications", None))
#if QT_CONFIG(tooltip)
        self.labelCheckInterval.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.labelCheckInterval.setText(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Interval minutes</p></body></html>", None))
#if QT_CONFIG(tooltip)
        self.spinBoxCheckInterval.setToolTip(QCoreApplication.translate("SettingsEdit", u"How often online streams are checked.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.spinBoxCheckInterval.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Define how often should we check what streams are online.</p><p>Default 30min.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.groupBox_2.setTitle(QCoreApplication.translate("SettingsEdit", u"Player", None))
#if QT_CONFIG(tooltip)
        self.checkBoxUseMobile.setToolTip(QCoreApplication.translate("SettingsEdit", u"Open mobile version of Twitch streams.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxUseMobile.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Use mobile version of Twitch links.</p><p>https://m.twitch.tv/&lt;name&gt; instead of the normal https://www.twitch.tv/&lt;name&gt;.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxUseMobile.setText(QCoreApplication.translate("SettingsEdit", u"Use mobile URL", None))
#if QT_CONFIG(tooltip)
        self.checkBoxPlayerSet.setToolTip(QCoreApplication.translate("SettingsEdit", u"Toggle on to set and use your own player command.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxPlayerSet.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Toggle Custom player ON to choose your own player command to start streams with. If Custom player is off Streamchecker will try to use your OS default webbrowser.</p><p>Custom player is required for Autostarting to be available. Autostarting doesen't work well with webbrowser as a player.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxPlayerSet.setText(QCoreApplication.translate("SettingsEdit", u"Custom player", None))
#if QT_CONFIG(tooltip)
        self.lineEditPlayerCommand.setToolTip(QCoreApplication.translate("SettingsEdit", u"Write your player command or add arguments for it.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.lineEditPlayerCommand.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Write your player command or add your additional at the end of the command arguments.</p><p>You can use the ... button at the right side to choose your player.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(tooltip)
        self.toolButtonPlayerChoose.setToolTip(QCoreApplication.translate("SettingsEdit", u"Find your video player program.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.toolButtonPlayerChoose.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Find your video player program.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.toolButtonPlayerChoose.setText(QCoreApplication.translate("SettingsEdit", u"...", None))
#if QT_CONFIG(tooltip)
        self.checkBoxAutostarting.setToolTip(QCoreApplication.translate("SettingsEdit", u"Choose which streams to start playing automatically if online.\n"
"Requires custom player and may not work with all players.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxAutostarting.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Automatically start playing if chosen streams come online.<br/><br/>Requires custom player and may not work with all players. For example webbrowsers propably will not work well with Autostarting.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxAutostarting.setText(QCoreApplication.translate("SettingsEdit", u"Autostarting", None))
#if QT_CONFIG(tooltip)
        self.pushButtonAutostarting.setToolTip(QCoreApplication.translate("SettingsEdit", u"Choose which streams to start playing automatically if online.\n"
"Requires custom player and may not work with all players.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.pushButtonAutostarting.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Pick the streams you want to automatically start playing when they come online. Use Ctrl+Left mouse button.<br/><br/>Requires custom player and may not work with all players. For example webbrowsers propably will not work well with Autostarting.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.pushButtonAutostarting.setText(QCoreApplication.translate("SettingsEdit", u"Streams", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("SettingsEdit", u"Chat", None))
#if QT_CONFIG(tooltip)
        self.checkBoxAlwaysChat.setToolTip(QCoreApplication.translate("SettingsEdit", u"Open chat with normal start stream functions.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxAlwaysChat.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Opens chat if a stream is opened with double click, Enter, tray icon or filter autoplay.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxAlwaysChat.setText(QCoreApplication.translate("SettingsEdit", u"Always open chat", None))
#if QT_CONFIG(tooltip)
        self.lineEditChatCommand.setToolTip(QCoreApplication.translate("SettingsEdit", u"Write command that opens chat.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.lineEditChatCommand.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Write your chat command or add your additional at the end of the command arguments.</p><p>You can use the ... button at the right side to choose your program.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(tooltip)
        self.toolButtonChatChoose.setToolTip(QCoreApplication.translate("SettingsEdit", u"Find program that opens chat.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.toolButtonChatChoose.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Find program that opens chat.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.toolButtonChatChoose.setText(QCoreApplication.translate("SettingsEdit", u"...", None))
#if QT_CONFIG(tooltip)
        self.checkBoxChatSet.setToolTip(QCoreApplication.translate("SettingsEdit", u"Toggle on to set and use your own command to open chat.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxChatSet.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Toggle Custom chat ON to choose your own chat command to start Twitch chat with. If Custom chat is off Streamchecker will try to use your OS default webbrowser.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxChatSet.setText(QCoreApplication.translate("SettingsEdit", u"Custom chat", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("SettingsEdit", u"&General", None))
#if QT_CONFIG(tooltip)
        self.labelX.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.labelX.setText(QCoreApplication.translate("SettingsEdit", u"width", None))
#if QT_CONFIG(tooltip)
        self.spinBoxX.setToolTip(QCoreApplication.translate("SettingsEdit", u"Adjust mainwindow width.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.spinBoxX.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Adjust main window width in pixels.</p><p>Default 350.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.groupBox.setTitle(QCoreApplication.translate("SettingsEdit", u"Icons", None))
#if QT_CONFIG(tooltip)
        self.checkBoxEnableTrayIcons.setToolTip(QCoreApplication.translate("SettingsEdit", u"Show icons in traymenu when right clicked.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxEnableTrayIcons.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Show icons in traymenu when right clicked.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxEnableTrayIcons.setText(QCoreApplication.translate("SettingsEdit", u"Enable traymenu icons", None))
#if QT_CONFIG(tooltip)
        self.checkBoxEnableIcons.setToolTip(QCoreApplication.translate("SettingsEdit", u"Enables streamer icons as a value in mainwindow table choices.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxEnableIcons.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Downloads streamer icons from Twitch.</p><p>Enables streamer icons as a column in &quot;Main window table&quot; choices.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxEnableIcons.setText(QCoreApplication.translate("SettingsEdit", u"Enable streamer icons", None))
#if QT_CONFIG(tooltip)
        self.checkBoxShowToolbarIcons.setToolTip(QCoreApplication.translate("SettingsEdit", u"Toggle between icons or text for toolbar actions.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxShowToolbarIcons.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Switch between icons and text for toolbar items.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxShowToolbarIcons.setText(QCoreApplication.translate("SettingsEdit", u"Show toolbar icons", None))
#if QT_CONFIG(tooltip)
        self.checkBoxShowTitlebar.setToolTip(QCoreApplication.translate("SettingsEdit", u"Toggle frameless mode.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxShowTitlebar.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Tries to change Streamer Retriever into frameless window.</p><p>This setting requires restarting Streamer Retriever before changes take effect.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxShowTitlebar.setText(QCoreApplication.translate("SettingsEdit", u"Frameless mode (Requires restart)", None))
#if QT_CONFIG(tooltip)
        self.checkBoxStartHidden.setToolTip(QCoreApplication.translate("SettingsEdit", u"Start Streamchecker GUI minimized.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxStartHidden.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Streamchecker starts with only tray icon showing.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxStartHidden.setText(QCoreApplication.translate("SettingsEdit", u"Start hidden", None))
#if QT_CONFIG(tooltip)
        self.checkBoxShowSidePanel.setToolTip(QCoreApplication.translate("SettingsEdit", u"Show side panel in main window.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxShowSidePanel.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Show side panel in main window.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxShowSidePanel.setText(QCoreApplication.translate("SettingsEdit", u"Show side panel", None))
#if QT_CONFIG(tooltip)
        self.checkBoxShowToolbar.setToolTip(QCoreApplication.translate("SettingsEdit", u"Hide the menubar. Ctrl+Shift+M to show|hide temporarily.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxShowToolbar.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Hides the topmost bar from mainwindow.</p><p>Ctrl+Shift+M to show|hide temporarily.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxShowToolbar.setText(QCoreApplication.translate("SettingsEdit", u"Show menubar", None))
#if QT_CONFIG(tooltip)
        self.spinBoxY.setToolTip(QCoreApplication.translate("SettingsEdit", u"Adjust mainwindow height.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.spinBoxY.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Adjust main window height in pixels.<br/><br/>Default 500.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.groupBox_3.setTitle(QCoreApplication.translate("SettingsEdit", u"Theme", None))
        self.label_8.setText(QCoreApplication.translate("SettingsEdit", u"Application theme (Requires restart)", None))
        self.label_3.setText(QCoreApplication.translate("SettingsEdit", u"Icon theme", None))
#if QT_CONFIG(tooltip)
        self.comboBoxTheme.setToolTip(QCoreApplication.translate("SettingsEdit", u"Change application theme colorset.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.comboBoxTheme.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Change application theme colorset.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(tooltip)
        self.checkBoxQssEnable.setToolTip(QCoreApplication.translate("SettingsEdit", u"Toogle to use your your own .qss theme file.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxQssEnable.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Toogle to use your your own .qss theme file.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxQssEnable.setText(QCoreApplication.translate("SettingsEdit", u"Theme from .qss file (Requires restart)", None))
#if QT_CONFIG(tooltip)
        self.toolButtonQssTheme.setToolTip(QCoreApplication.translate("SettingsEdit", u"Choose your .qss theme file.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.toolButtonQssTheme.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Choose your .qss theme file.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.toolButtonQssTheme.setText(QCoreApplication.translate("SettingsEdit", u"...", None))
#if QT_CONFIG(tooltip)
        self.comboBoxIconTheme.setToolTip(QCoreApplication.translate("SettingsEdit", u"Choose icon theme for mainwindow.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.comboBoxIconTheme.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Change icon theme for mainwindow.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(tooltip)
        self.checkBoxQssDefault.setToolTip(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Default dark theme.</p></body></html>", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxQssDefault.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Default dark theme inspired by Nord theme.</p><p>Made for Fusion but might work with other application themes.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxQssDefault.setText(QCoreApplication.translate("SettingsEdit", u"Stonewall-dark theme (Requires restart, made for Fusion)", None))
#if QT_CONFIG(tooltip)
        self.label_6.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.label_6.setText(QCoreApplication.translate("SettingsEdit", u"Window size:", None))
#if QT_CONFIG(tooltip)
        self.labelY.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.labelY.setText(QCoreApplication.translate("SettingsEdit", u"height", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), QCoreApplication.translate("SettingsEdit", u"&UI", None))
        self.groupBoxFilter.setTitle(QCoreApplication.translate("SettingsEdit", u"Table", None))
#if QT_CONFIG(tooltip)
        self.label_4.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.label_4.setText(QCoreApplication.translate("SettingsEdit", u"Margin", None))
        self.label_10.setText(QCoreApplication.translate("SettingsEdit", u"row", None))
#if QT_CONFIG(tooltip)
        self.label.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.label.setText(QCoreApplication.translate("SettingsEdit", u"Main window table columns", None))
#if QT_CONFIG(tooltip)
        self.label_2.setToolTip("")
#endif // QT_CONFIG(tooltip)
        self.label_2.setText(QCoreApplication.translate("SettingsEdit", u"Choose stretch column", None))
#if QT_CONFIG(tooltip)
        self.label_5.setToolTip("")
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.label_5.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Sets row height for main window table items.</p><p>Also sets the icon size if icons are enabled and chosen as a column value.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.label_5.setText(QCoreApplication.translate("SettingsEdit", u"Row height", None))
#if QT_CONFIG(tooltip)
        self.spinBoxRowHeight.setToolTip(QCoreApplication.translate("SettingsEdit", u"Set main window table row height in pixels.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.spinBoxRowHeight.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Sets row height for main window table items.</p><p>Also sets the icon size if icons are enabled and chosen from available columns.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(tooltip)
        self.spinBoxMarginRow.setToolTip(QCoreApplication.translate("SettingsEdit", u"Margin between main window rows.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.spinBoxMarginRow.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Inputs margin between main window table rows.</p><p>Mostly usefull with streamer icons.<br/><br/>Default 4.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
#if QT_CONFIG(tooltip)
        self.pushButtonShowColumns.setToolTip(QCoreApplication.translate("SettingsEdit", u"Choose items to show in main window.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.pushButtonShowColumns.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Choose what info should be shown in main window from streamers.</p><p>Use Ctrl+Left mouse button to choose columns in the order you want them to be shown.</p><p>Default dispName, game.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.pushButtonShowColumns.setText(QCoreApplication.translate("SettingsEdit", u"Columns", None))
#if QT_CONFIG(tooltip)
        self.checkBoxColumnHeader.setToolTip(QCoreApplication.translate("SettingsEdit", u"Show column header names in Main Window table.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxColumnHeader.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Show column header names in Main Window table.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxColumnHeader.setText(QCoreApplication.translate("SettingsEdit", u"Show column headers", None))
#if QT_CONFIG(tooltip)
        self.checkBoxRowHeader.setToolTip(QCoreApplication.translate("SettingsEdit", u"Show labels in main window table.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxRowHeader.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Display column labels in mainwindow table.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxRowHeader.setText(QCoreApplication.translate("SettingsEdit", u"Show row header", None))
#if QT_CONFIG(tooltip)
        self.checkBoxShowGrid.setToolTip(QCoreApplication.translate("SettingsEdit", u"Show grid in mainwindow table.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxShowGrid.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Show grid in Main Window table.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxShowGrid.setText(QCoreApplication.translate("SettingsEdit", u"Show grid", None))
#if QT_CONFIG(tooltip)
        self.pushButtonStretchColumn.setToolTip(QCoreApplication.translate("SettingsEdit", u"Choose which of the mainwindow item columns should be streched to fit window.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.pushButtonStretchColumn.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Choose column that will be stretched to fit main window width.<br/><br/>Default game.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.pushButtonStretchColumn.setText(QCoreApplication.translate("SettingsEdit", u"Columns", None))
        self.groupBoxUi.setTitle(QCoreApplication.translate("SettingsEdit", u"Filter", None))
#if QT_CONFIG(tooltip)
        self.checkBoxFilterAuto.setToolTip(QCoreApplication.translate("SettingsEdit", u"Start playing stream if filter is used and there are only one stream left.", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.checkBoxFilterAuto.setWhatsThis(QCoreApplication.translate("SettingsEdit", u"<html><head/><body><p>Autoplay will automatically start playing stream when you use the filter and the filter value matches only one stream.</p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.checkBoxFilterAuto.setText(QCoreApplication.translate("SettingsEdit", u"Filter autoplay", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("SettingsEdit", u"&Streams Table", None))
    # retranslateUi

