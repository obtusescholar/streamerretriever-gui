#! /usr/bin/env python3


import os
import sys
from pathlib import Path


from PySide2.QtWidgets import (
    QDialogButtonBox,
    QApplication,
    QDialog)
from PySide2.QtCore import (
    QPoint,
    QUrl,
    Qt)
from PySide2.QtGui import (
    QTextDocument,
    QPixmap,
    QIcon)


from . ui_changelog import Ui_Changelog


class Changelog(QDialog):
    def __init__(self, api, parent=None):
        super().__init__(parent)
        self.__api = api
        self.__conf = self.__api.getConfigs()

        window_icon = self.__api.getWindowIcons()[0]
        self.setWindowIcon(QIcon(QPixmap(window_icon)))
        self.setWindowFlag(
            Qt.FramelessWindowHint, self.__conf.showTitlebar)

        self.ui = Ui_Changelog()
        self.ui.setupUi(self)
        self.ui.buttonBox.clicked.connect(self.__handleButtons)

        base = Path(__file__).parents[1]
        path = Path(base) / Path('CHANGELOG.md')
        url = QUrl(str(path), type=QTextDocument.MarkdownResource)
        self.ui.textBrowserChangelog.setSource(
            url, QTextDocument.MarkdownResource)

        self.show()

    def __handleButtons(self, button):
        sb = self.ui.buttonBox.standardButton(button)
        if sb == QDialogButtonBox.Ok:
            self.accept()

    def mousePressEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            self.oldPos = event.globalPos()
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            delta = QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()
        else:
            super().mouseMoveEvent(event)


def main():
    app = QApplication([])
    widget = Changelog()
    widget.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
