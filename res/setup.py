#! /usr/bin/env python3


"""cx_freeze binary script

run with:
python setup.py build
python setup.py bdist_msi"""


import sys
from pathlib import Path

from streamerretriever_gui import constants

from cx_Freeze import (
    Executable,
    setup)


relative = Path('~/AppData/Roaming/Microsoft/Windows/Start Menu/Programs')
absolute = Path.resolve(Path.expanduser(relative))
pkgname = 'streamerretriever-gui'

include_files = [
    ('res/themes/stonewall-dark.qss', 'res/themes/stonewall-dark.qss'),
    'CHANGELOG.md',
    'LICENSE',
    'README.md']
excludes = ['tkinter']

# Dependencies are automatically detected, but it might need
# fine tuning.
build_options = {
    'packages': [],
    'excludes': excludes,
    'include_files': include_files,
    'build_exe': 'build/' + pkgname + '-' + constants.__version__,
    'silent': True}

bdist_msi_options = {
    'install_icon': 'res/streamerretriever.ico',
    'summary_data': {
        'author': 'obtusescholar',
        'comments': 'Retrieves online streams.'}}

base = 'Win32GUI' if sys.platform=='win32' else None

executables = [
    Executable(
        pkgname + '.py',
        base=base,
        target_name = 'Streamer Retriever-GUI',
        icon = 'res/streamerretriever.ico',
        # Causes error: 2756
        # shortcut_name = 'Streamer Retriever-GUI.lnk',
        shortcut_dir = str(absolute))]


setup(
    name = 'Streamer Retriever-GUI',
    version = constants.__version__,
    description = ('Retrieves online streams.'),
    options = {
        'build_exe': build_options,
        'build_msi': bdist_msi_options},
    executables = executables)
