#! /usr/bin/env python3


import os
import sys
import logging

from PySide2.QtWidgets import (
    QApplication, QDialog, QTableWidgetItem, QHeaderView, QAbstractItemView,
    QTableWidget, QDialogButtonBox, QTableWidgetSelectionRange)
from PySide2.QtCore import Qt, QPoint

from . import constants
from . import tools_gui


logger = logging.getLogger(__name__)
logger.setLevel(constants.LOG_LEVEL)

file_handler = logging.FileHandler(constants.CHOOSER_LOG)
formatter = logging.Formatter(constants.CHOOSER_FORM)
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class Chooser(QDialog):
    def __init__(
            self, data, orientation, drag, select, conf, *ignore, parent=None):
        super().__init__(parent)
        self.__parent = parent
        self.__conf = conf
        self.__data = data
        self.__drag = drag

        if drag and orientation == 'row':
            self.ui = tools_gui.Ui_Chooser(tools_gui.DragColumns)
        else:
            self.ui = tools_gui.Ui_Chooser(QTableWidget)

        self.ui.setupUi(self)
        self.ui.buttonBox.clicked.connect(self.__handleButtons)

        self.__table(orientation)
        if select:
            selection = QTableWidgetSelectionRange(
                select[0], select[1], select[2], select[3])
            self.ui.tableWidget.setRangeSelected(selection, True)

        self.setWindowFlag(
            Qt.FramelessWindowHint, self.__conf.showTitlebar)

        self.show()

    def __handleButtons(self, button):
        sb = self.ui.buttonBox.standardButton(button)
        if sb == QDialogButtonBox.Cancel:
            self.reject()
        elif sb == QDialogButtonBox.Ok:
            selected = self.ui.tableWidget.selectedItems()

            if self.__drag:
                items = self.__sortDragged(selected)
            else:
                items = [x.text() for x in selected]

            self.__parent.setChooserList(items)
            self.accept()

    def __sortDragged(self, selected):
        indexes = {}
        for s in selected:
            i = str(self.ui.tableWidget.indexFromItem(s).column())
            indexes[i] = s.text()

        items = [indexes[i] for i in sorted(indexes)]
        return items

    def __tableSettings(self, orientation):
        self.ui.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        if orientation == 'row':
            self.ui.tableWidget.setRowCount(len(self.__data))
            self.ui.tableWidget.setColumnCount(len(self.__data[0]))
        elif orientation == 'column':
            self.ui.tableWidget.setRowCount(len(self.__data[0]))
            self.ui.tableWidget.setColumnCount(len(self.__data))
        else:
            raise ValueError('Unkown table data orientation value.')

    def __table(self, orientation):
        self.__tableSettings(orientation)

        self.ui.tableWidget.setHorizontalScrollMode(
            QAbstractItemView.ScrollPerPixel)

        header = self.ui.tableWidget.horizontalHeader()
        header.setVisible(True)
        self.ui.tableWidget.verticalHeader().setVisible(True)

        for y, row in enumerate(self.__data):
            if orientation == 'column':
                # header.setSectionResizeMode(y, QHeaderView.ResizeToContents)
                header.setSectionResizeMode(y, QHeaderView.Stretch)
            for x, column in enumerate(row):
                item = QTableWidgetItem(column)

                if orientation == 'row':
                    header.setSectionResizeMode(
                        x, QHeaderView.ResizeToContents)
                    # header.setSectionResizeMode(x, QHeaderView.Stretch)
                    self.ui.tableWidget.setItem(y, x, item)
                elif orientation == 'column':
                    self.ui.tableWidget.setItem(x, y, item)

    def mousePressEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            self.oldPos = event.globalPos()
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if os.name == 'nt' and self.__conf.showTitlebar:
            delta = QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()
        else:
            super().mouseMoveEvent(event)


def main():
    # data = [
    #     ['a', 'b', 'c', 'd', 'e'],
    #     ['f', 'g', 'h', 'i', 'j']]
    data = [['a', 'b', 'c', 'd', 'e']]

    # orientation = 'column'
    orientation = 'row'

    # drag = True
    drag = False

    app = QApplication([])
    widget = Chooser(data, orientation, drag)
    widget.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
