#! /usr/bin/env python3


import os
import sys

from PySide2.QtWidgets import (
    QApplication, QDialog, QStyledItemDelegate, QAbstractItemView,
    QPushButton, QDialogButtonBox, QHeaderView)
from PySide2.QtCore import QModelIndex, QPoint
from PySide2.QtGui import Qt, QStandardItem, QStandardItemModel, QIcon, QPixmap
from PySide2 import QtCore

from . ui_tableview import Ui_Dialog

from . import constants


class PushButtonDelegate(QStyledItemDelegate):
    clicked = QtCore.Signal(QModelIndex)

    def paint(self, painter, option, index):
        rules = [
            isinstance(self.parent(), QAbstractItemView),
            self.parent().model() is index.model()]

        if all(rules):
            self.parent().openPersistentEditor(index)

    def createEditor(self, parent, option, index):
        button = QPushButton(parent)
        button.clicked.connect(lambda *args, ix=index: self.clicked.emit(ix))
        return button

    def setEditorData(self, editor, index):
        editor.setText(index.data(Qt.DisplayRole))

    def setModelData(self, editor, model, index):
        pass


class TwitchLoad(QDialog):

    def __init__(self, api, *ignore, parent=None):
        super().__init__(parent)
        self.__parent = parent
        self.__api = api
        self.__conf = self.__api.getConfigs()

        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.ui.buttonBox.clicked.connect(self.__handleButtons)
        self.ui.tableView.setEditTriggers(QAbstractItemView.NoEditTriggers)

        self.__follows = list(self.__api.getFollows())
        self.__cells = self.__createCells()
        self.__populateTable(self.__cells)
        self.__buttons()

        self.setWindowFlag(
            Qt.FramelessWindowHint, not self.__conf.showTitlebar)

    def __handleButtons(self, button):
        sb = self.ui.buttonBox.standardButton(button)
        if sb == QDialogButtonBox.Ok:
            additions = []
            for c in self.__cells:
                if c.data(Qt.CheckStateRole):
                    additions.append(c.data(role=Qt.DisplayRole))

            additions.extend(self.__follows)
            self.__api.updateFollows(additions)

    def __selectAll(self):
        for item in self.__cells:
            item.setData(Qt.Checked, Qt.CheckStateRole)

    def __selectNone(self):
        for item in self.__cells:
            item.setData(Qt.Unchecked, Qt.CheckStateRole)

    def __buttons(self):
        t = self.__conf.iconTheme

        plus = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/plus.svg'))
        self.ui.pushButton1.setIcon(plus)
        self.ui.pushButton1.clicked.connect(self.__selectAll)

        minus = QIcon(QPixmap(
            u':/' + constants.ICONS_BUTTONS + t + '/minus.svg'))
        self.ui.pushButton2.setIcon(minus)
        self.ui.pushButton2.clicked.connect(self.__selectNone)

    def __createCells(self):
        twitch_follows = self.__api.getTwitchFollows()
        twitch_names = tuple(t.getName() for t in twitch_follows)
        items = []
        for follow in twitch_names:
            if follow in self.__follows:
                continue
            item = QStandardItem()
            item.setData(f'{follow}', Qt.DisplayRole)
            item.setData(Qt.Checked, Qt.CheckStateRole)
            item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
            items.append(item)
        return items

    def __populateTable(self, tabular_data):
        model = QStandardItemModel(0, 1)

        for item in tabular_data:
            model.appendRow(item)
            # model.appendRow([i[0], i[1]])

        self.ui.tableView.setModel(model)
        self.ui.tableView.setSelectionMode(QAbstractItemView.NoSelection)
        self.ui.tableView.setShowGrid(False)
        header = self.ui.tableView.horizontalHeader()
        header.setSectionResizeMode(0, QHeaderView.Stretch)
        header.setVisible(False)
        self.ui.tableView.verticalHeader().setVisible(False)

    def mousePressEvent(self, event):
        if os.name == 'nt' and not self.__conf.showTitlebar:
            self.oldPos = event.globalPos()
        else:
            super().mousePressEvent(event)

    def mouseMoveEvent(self, event):
        if os.name == 'nt' and not self.__conf.showTitlebar:
            delta = QPoint(event.globalPos() - self.oldPos)
            self.move(self.x() + delta.x(), self.y() + delta.y())
            self.oldPos = event.globalPos()
        else:
            super().mouseMoveEvent(event)


def main():
    app = QApplication([])
    widget = TwitchLoad()
    widget.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
