# -*- mode: python ; coding: utf-8 -*-

# pip install pyinstaller PySide2 pywin32

from pathlib import Path

block_cipher = None
path = Path.resolve(Path.expanduser(Path('~/git/streamerretriever-gui')))


a = Analysis(['streamerretriever-gui.py'],
             pathex=[path],
             binaries=[],
             datas=[('res/themes/stonewall-dark.qss', 'res/themes')],
             hiddenimports=['PySide2.QtXml', 'streamerretriever'],
             hookspath=[],
             runtime_hooks=[],
             excludes=['streamerretriever/__pycache__', 'streamerretriever_gui/__pycache__'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='streamerretriever-gui',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=False,
          console=False , icon='res/streamerretriever.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               upx_exclude=[],
               name='Streamer Retriever-GUI')
