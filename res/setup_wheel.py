#! /usr/bin/env python3


from streamchecker_gui import constants
from setuptools import setup, find_packages


with open('README.md', 'r') as fh:
    long_description = fh.read()

setup(
    name='Streamchecker-GUI',
    version=constants.__version__,
    author=constants.__author__,
    description='Checks which streams are online from your follows list and allows starting a stream with player of your choice.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/noobilanderi/streamchecker-gui',
    entry_points={
        'gui_scripts': ['streamchecker-gui = streamchecker_gui.gui:main']
    },
    packages=find_packages(
        include=['streamchecker_gui'],
        exclude=['streamchecker_gui/__pycache__']
    ),
    package_data={
        'icons': ['*.svg']
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Operating System :: OS Independent',
        'Intended Audience :: End Users/Desktop',
        'Programming Language :: Python :: 3.8',
        'Topic :: Internet',
        'Topic :: Terminals',
        'Topic :: Utilities'
    ],
    license='GPLv3+',
    keywords='twitch stream play',
    python_requires='>=3.8',
    install_requires=['streamchecker', 'PySide2']
)
